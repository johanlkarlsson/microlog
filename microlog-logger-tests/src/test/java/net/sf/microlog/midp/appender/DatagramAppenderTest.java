package net.sf.microlog.midp.appender;

import junit.framework.TestCase;
import net.sf.microlog.core.Appender;

public class DatagramAppenderTest extends TestCase {

	private DatagramAppender appenderToTest;

	protected void setUp() throws Exception {
		super.setUp();
		appenderToTest = new DatagramAppender();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testSetHostNull() {
		try {
			appenderToTest.setHost(null);
			fail("An IllegalArgumentException was expected.");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}
	}

	public void testSetEncodingNull() {

		try {
			appenderToTest.setEncoding(null);
			fail("An IllegalArgumentException was expected.");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}
	}

	public void testSetEncodingZeroLength() {

		try {
			appenderToTest.setEncoding("");
			fail("An IllegalArgumentException was expected.");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}
	}

	/**
	 * Test method for
	 * {@link net.sf.microlog.midp.appender.DatagramAppender#getLogSize()}.
	 */
	public void testGetLogSize() {
		assertEquals("", Appender.SIZE_UNDEFINED, appenderToTest.getLogSize());
	}
}
