/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.midp.appender;

import junit.framework.TestCase;
import net.sf.microlog.core.SyslogMessage;

/**
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 * 
 */
public class SyslogAppenderTest extends TestCase {

	private SyslogAppender appenderToTest;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		appenderToTest = new SyslogAppender();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testDefaultSyslogPort() {
		assertEquals("The default syslog port was not correct.", 514,
				appenderToTest.port);
	}

	public void testSetFacility() {
		appenderToTest.setFacility(SyslogMessage.FACILITY_LOG_AUDIT);

		assertEquals("The facility was not set correct.",
				SyslogMessage.FACILITY_LOG_AUDIT, appenderToTest.syslogMessage.getFacility());
	}

	public void testSetFacilityNegative() {
		try {
			appenderToTest.setFacility((byte) -1);
			fail("An exception was expected.");
		} catch (IllegalArgumentException e) {
			// This is expected
			assertTrue(true);
		}
	}

	public void testSetFacilityToHigh() {
		try {
			appenderToTest.setFacility((byte) 24);
			fail("An exception was expected.");
		} catch (IllegalArgumentException e) {
			// This is expected
			assertTrue(true);
		}
	}

	public void testSetSeverityNegative() {
		try {
			appenderToTest.setSeverity((byte) -1);
			fail("An exception was expected.");
		} catch (IllegalArgumentException e) {
			// This is expected.
			assertTrue(true);
		}
	}

	public void testSeverityToHigh() {
		try {
			appenderToTest.setSeverity((byte) 8);
			fail("An exception was expected.");
		} catch (IllegalArgumentException e) {
			// This is expected.
			assertTrue(true);
		}
	}

	public void testSetHostname() {

		appenderToTest.setHostname("192.168.0.2");

		assertNotNull("The hostname must not be null", appenderToTest.host);
		assertEquals("Not the correct hostname", "192.168.0.2",
				appenderToTest.syslogMessage.getHostname());
	}

	public void testSetHostnameNull() {

		try {
			appenderToTest.setHostname(null);
			fail("An exception was expected.");
		} catch (IllegalArgumentException e) {
			// This is expected
			assertTrue(true);
		}
	}

	public void testGetDefaultHostname() {
		assertEquals("Not the correct default hostname", "127.0.0.1",
				appenderToTest.hostname);
	}

	public void testSetTagNull() {

		try {
			appenderToTest.setTag(null);
			fail("An exception was expected");
		} catch (IllegalArgumentException e) {
			// This is expected
			assertTrue(true);
		}
	}
}
