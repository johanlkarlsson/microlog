/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.midp;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import javax.microedition.rms.RecordComparator;

import net.sf.microlog.midp.AscendingComparator;

import junit.framework.TestCase;

/**
 * Class to test the <code>AscendingComparator</code>.
 * 
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class AscendingComparatorTest extends TestCase {

	private ByteArrayOutputStream baos = new ByteArrayOutputStream();
	private DataOutputStream dos = new DataOutputStream(baos);

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testCompareEquivalent() throws IOException {

		// Prepare the data to compare
		dos.writeLong(1000);
		byte[] data1 = baos.toByteArray();
		baos.reset();
		dos.writeLong(1000);
		byte[] data2 = baos.toByteArray();

		AscendingComparator comparator = new AscendingComparator();

		int comparedValue = comparator.compare(data1, data2);

		assertEquals("Not the correct value", RecordComparator.EQUIVALENT,
				comparedValue);

	}

	public void testCompareEquivalentSmallValues() throws IOException {

		// Prepare the data to compare
		dos.writeLong(0);
		byte[] data1 = baos.toByteArray();
		baos.reset();
		dos.writeLong(0);
		byte[] data2 = baos.toByteArray();

		AscendingComparator comparator = new AscendingComparator();

		int comparedValue = comparator.compare(data1, data2);

		assertEquals("Not the correct value", RecordComparator.EQUIVALENT,
				comparedValue);

	}

	public void testCompareEquivalentBigValues() throws IOException {

		// Prepare the data to compare
		dos.writeLong(Long.MAX_VALUE);
		byte[] data1 = baos.toByteArray();
		baos.reset();
		dos.writeLong(Long.MAX_VALUE);
		byte[] data2 = baos.toByteArray();

		AscendingComparator comparator = new AscendingComparator();

		int comparedValue = comparator.compare(data1, data2);

		assertEquals("Not the correct value", RecordComparator.EQUIVALENT,
				comparedValue);

	}

	public void testComparePreceds() throws IOException {

		// Prepare the data to compare
		dos.writeLong(500);
		byte[] data1 = baos.toByteArray();
		baos.reset();
		dos.writeLong(1000);
		byte[] data2 = baos.toByteArray();

		AscendingComparator comparator = new AscendingComparator();

		int comparedValue = comparator.compare(data1, data2);

		assertEquals("Not the correct value", RecordComparator.PRECEDES,
				comparedValue);

	}

	public void testComparePrecedsSmallValues() throws IOException {

		// Prepare the data to compare
		dos.writeLong(0);
		byte[] data1 = baos.toByteArray();
		baos.reset();
		dos.writeLong(1);
		byte[] data2 = baos.toByteArray();

		AscendingComparator comparator = new AscendingComparator();

		int comparedValue = comparator.compare(data1, data2);

		assertEquals("Not the correct value", RecordComparator.PRECEDES,
				comparedValue);

	}
	
	public void testComparePrecedsBigValues() throws IOException {

		// Prepare the data to compare
		dos.writeLong(Long.MAX_VALUE-1);
		byte[] data1 = baos.toByteArray();
		baos.reset();
		dos.writeLong(Long.MAX_VALUE);
		byte[] data2 = baos.toByteArray();

		AscendingComparator comparator = new AscendingComparator();

		int comparedValue = comparator.compare(data1, data2);

		assertEquals("Not the correct value", RecordComparator.PRECEDES,
				comparedValue);

	}

	public void testCompareFollows() throws IOException {
		// Prepare the data to compare
		dos.writeLong(1000);
		byte[] data1 = baos.toByteArray();
		baos.reset();
		dos.writeLong(500);
		byte[] data2 = baos.toByteArray();

		AscendingComparator comparator = new AscendingComparator();

		int comparedValue = comparator.compare(data1, data2);

		assertEquals("Not the correct value", RecordComparator.FOLLOWS,
				comparedValue);
	}
	
	public void testCompareFollowsSmallValues() throws IOException {
		// Prepare the data to compare
		dos.writeLong(1);
		byte[] data1 = baos.toByteArray();
		baos.reset();
		dos.writeLong(0);
		byte[] data2 = baos.toByteArray();

		AscendingComparator comparator = new AscendingComparator();

		int comparedValue = comparator.compare(data1, data2);

		assertEquals("Not the correct value", RecordComparator.FOLLOWS,
				comparedValue);
	}
	
	public void testCompareFollowsBigValues() throws IOException {
		// Prepare the data to compare
		dos.writeLong(Long.MAX_VALUE);
		byte[] data1 = baos.toByteArray();
		baos.reset();
		dos.writeLong(Long.MAX_VALUE-1);
		byte[] data2 = baos.toByteArray();

		AscendingComparator comparator = new AscendingComparator();

		int comparedValue = comparator.compare(data1, data2);

		assertEquals("Not the correct value", RecordComparator.FOLLOWS,
				comparedValue);
	}

}
