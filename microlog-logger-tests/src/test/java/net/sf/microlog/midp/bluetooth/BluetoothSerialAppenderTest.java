package net.sf.microlog.midp.bluetooth;

import java.io.IOException;

import net.sf.microlog.core.Level;
import net.sf.microproperties.Properties;

import junit.framework.TestCase;

public class BluetoothSerialAppenderTest extends TestCase {
	private BluetoothSerialAppender bluetoothSerialAppender;
	private MockBluetoothConnectionHandlerImpl mockConnectionHandler;
	
	protected void setUp() throws Exception {
		mockConnectionHandler = new MockBluetoothConnectionHandlerImpl();
		bluetoothSerialAppender = new BluetoothSerialAppender(mockConnectionHandler);
	}
	
	public void testClose() throws IOException {
		bluetoothSerialAppender.close();

		assertTrue(mockConnectionHandler.closeMethodCalled);
		assertFalse(bluetoothSerialAppender.isLogOpen());
	}
	
	public void testShutdownLoggingService() throws IOException {
		bluetoothSerialAppender.shutdownLoggingService();
		
		assertTrue(mockConnectionHandler.shutdownLoggingServiceCalled);
		assertFalse(bluetoothSerialAppender.isLogOpen());
	}
	
	public void testDoLog() {
		bluetoothSerialAppender.doLog("clientId", "name", System.currentTimeMillis(), Level.INFO, "This is from a JUnit test", null);
		
		assertTrue(mockConnectionHandler.writeLogToStreamCalled);
	}
	
	public void testOpen() throws IOException {
		bluetoothSerialAppender.open();
		
		assertTrue(mockConnectionHandler.openConnectionCalled);
	}
	
	public void testConfigure() {
		Properties properties = new Properties();
		properties.setProperty(BluetoothSerialAppender.SERVER_URL_STRING, "This is from a JUnit test");
		
		bluetoothSerialAppender.configure(properties);
		
		assertTrue(mockConnectionHandler.setConnectionStringCalled);
	}
}
