package net.sf.microlog.midp.bluetooth;

import java.io.IOException;

public class MockBluetoothConnectionHandlerImpl implements BluetoothConnectionHandler {
	boolean setConnectionStringCalled = false;
	boolean closeMethodCalled = false;
	boolean getBluetoothClientIDCalled = false;
	boolean openConnectionCalled = false;
	boolean shutdownLoggingServiceCalled = false;
	boolean writeLogToStreamCalled = false;
	
	// Difficult to test since the implementation is reliant on Bluetooth classes
	public void findAndSetConnectionString(final BluetoothRemoteDevice remoteDevice) {
	}
	
	public void setConnectionString(final String serverUrl) {
		setConnectionStringCalled = true;
	}
	
	public void close() throws IOException {
		closeMethodCalled = true;
	}

	public String getBluetoothClientID(final String clientID) {
		getBluetoothClientIDCalled = true;
		
		return "TestID";
	}

	public boolean openConnection() {
		openConnectionCalled = true;
		
		return true;
	}

	public void shutdownLoggingService() throws IOException {
		shutdownLoggingServiceCalled = true;
	}

	public void writeLogToStream(String formattedLogStatement) {
		writeLogToStreamCalled = true;
	}
}
