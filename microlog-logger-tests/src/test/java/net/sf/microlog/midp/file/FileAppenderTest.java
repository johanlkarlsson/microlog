package net.sf.microlog.midp.file;

import junit.framework.TestCase;

public class FileAppenderTest extends TestCase {

	private FileAppender fileAppender;

	protected void setUp() throws Exception {
		super.setUp();
		fileAppender = new FileAppender();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testSetFilenameProperty() {

	}

	public void testSetPropertySeparator() {

		fileAppender.setProperty(FileAppender.LINE_SEPARATOR_PROPERTY,
				"\r\n");

		assertEquals("Not the right lineseparator", "\r\n", fileAppender
				.getLineSeparator());
	}

	public void testSetPropertySeparatorNull() {

		try {
			fileAppender.setProperty(
					FileAppender.LINE_SEPARATOR_PROPERTY, null);
			fail("It was expected that an IllegalArgumentException ws thrown.");
		} catch (IllegalArgumentException e) {
			System.out
					.println("Catched an IllegalArgumentException as expected.");
		}
	}

	public void testSetPropertySeparatorLFOnly() {

		fileAppender.setProperty(FileAppender.LINE_SEPARATOR_PROPERTY,
				"\n");

		assertEquals("Not the right lineseparator", "\n", fileAppender
				.getLineSeparator());
	}

}
