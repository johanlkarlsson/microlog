/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.microlog.core.format.command;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.format.command.NoFormatCommand;
import junit.framework.TestCase;

/**
 * Class to test the <code>NoFormatCommand</code>.
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class NoFormatCommandTest extends TestCase {

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testExecute() {
		NoFormatCommand command = new NoFormatCommand();
		command.init("preFormattedString");

		assertEquals("Not the correct String", "preFormattedString", command
				.execute(null, "", 10, Level.INFO, "message", new Exception("Exception")));
	}
}
