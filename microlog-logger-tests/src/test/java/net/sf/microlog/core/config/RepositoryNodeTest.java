/*
 * Copyright 2009 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.microlog.core.config;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.Logger;
import net.sf.microlog.core.config.RepositoryNode;
import junit.framework.TestCase;

/**
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 *
 */
public class RepositoryNodeTest extends TestCase {

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testAddChild(){
		RepositoryNode rootNode = new RepositoryNode("root");
		RepositoryNode childNode = new RepositoryNode("child");
		
		rootNode.addChild(childNode);
		
		assertNotNull("The children Hashtable must no be null", rootNode.children);
		assertEquals("Not the correct number of children", 1, rootNode.children.size());
		assertSame("Not the same node", childNode, rootNode.getChildNode("child"));
	}
	
	public void testGetLevel(){
		Logger logger = new Logger("name");
		logger.setLevel(Level.TRACE);
		RepositoryNode node = new RepositoryNode("name", logger);
		
		Level level = node.getLevel();
		
		assertNotNull("The level must not be null.", level);
		assertEquals("Not the correct level.", Level.TRACE, level);
	}
	
	public void testGetLevelSpecifiedLevel(){
		RepositoryNode node = new RepositoryNode("name");
		node.level = Level.FATAL;
		
		Level level = node.getLevel();
		
		assertNotNull("The level must not be null.", level);
		assertEquals("Not the correct level.", Level.FATAL, level);
	}
	
	public void testGetLevelLoggerAndSpecifiedLevel(){
		Logger logger = new Logger("name");
		logger.setLevel(Level.TRACE);
		RepositoryNode node = new RepositoryNode("name", logger);
		node.level = Level.FATAL;
		
		Level level = node.getLevel();
		
		assertNotNull("The level must not be null.", level);
		assertEquals("Not the correct level.", Level.TRACE, level);
	}
}
