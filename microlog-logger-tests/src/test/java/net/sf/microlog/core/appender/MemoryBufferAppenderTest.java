package net.sf.microlog.core.appender;

import java.io.IOException;
import java.util.Vector;

import junit.framework.TestCase;
import net.sf.microlog.core.Level;

public class MemoryBufferAppenderTest
	extends TestCase
{
	public void testDefaultConstructor()
	{
		MemoryBufferAppender appender = new MemoryBufferAppender();
		assertEquals(20, appender.getMaxNbrOfEntries());
		assertEquals(true, appender.isCyclicBuffer());
	}
	
	public void testCustomConstructor()
	{
		MemoryBufferAppender appender = new MemoryBufferAppender(5, false);
		assertEquals(5, appender.getMaxNbrOfEntries());
		assertEquals(false, appender.isCyclicBuffer());
	}

	public void testCustomConstructorWithZeroBuffer()
	{
		MemoryBufferAppender appender = new MemoryBufferAppender(0, false);
		assertEquals(20, appender.getMaxNbrOfEntries());
		assertEquals(false, appender.isCyclicBuffer());
	}
	
	public void testConfigureEmpty()
	{
		MemoryBufferAppender appender = new MemoryBufferAppender();
		assertEquals(true, appender.isCyclicBuffer());
		assertEquals(20, appender.getMaxNbrOfEntries());
	}
	
	public void testLoggingCyclicBuffer() throws IOException
	{
		MemoryBufferAppender appender = new MemoryBufferAppender(2, true);
		appender.open();
		appender.doLog(null, "name", 10, Level.INFO, "message 1", null);
		appender.doLog(null, "name", 20, Level.ERROR, "message 2", null);
		assertEquals(2, appender.getLogSize());
		appender.doLog(null, "name", 30, Level.INFO, "message 3", null);
		appender.doLog(null, "name", 40, Level.ERROR, "message 4", null);
		appender.doLog(null, "name", 50, Level.DEBUG, "message 5", null);
		assertEquals(2, appender.getLogSize());
		assertTrue(((String)appender.getLogBuffer().elementAt(1)).endsWith("message 4"));
		assertTrue(((String)appender.getLogBuffer().elementAt(0)).endsWith("message 5"));
		appender.close();
	}
	
	public void testLogSize() throws IOException{
		MemoryBufferAppender appender = new MemoryBufferAppender(10, true);
		appender.open();
		appender.doLog(null, "name", 10, Level.INFO, "message 1", null);
		appender.doLog(null, "name", 20, Level.ERROR, "message 2", null);
		appender.doLog(null, "name", 30, Level.INFO, "message 1", null);
		appender.doLog(null, "name", 40, Level.ERROR, "message 2", null);
		System.out.println("appender logsize "+appender.getLogSize());
		assertEquals("Not the correct log size", 4, appender.getLogSize());
		Vector logBuffer = appender.getLogBuffer();
		assertNotNull("The log buffer must not be null.", logBuffer);
		System.out.println("logBuffer.size() "+logBuffer.size());
		assertEquals("Not the correct vector size", 4, logBuffer.size());
	}
	
	public void testLoggingFixedBuffer() throws IOException
	{
		MemoryBufferAppender appender = new MemoryBufferAppender(2, false);
		appender.open();
		appender.doLog(null, "name", 10, Level.INFO, "message 1", null);
		appender.doLog(null, "name", 20, Level.ERROR, "message 2", null);
		assertEquals(2, appender.getLogSize());
		appender.doLog(null, "name", 30, Level.INFO, "message 3", null);
		appender.doLog(null, "name", 40, Level.ERROR, "message 4", null);
		appender.doLog(null, "name", 50, Level.DEBUG, "message 5", null);
		assertEquals(2, appender.getLogSize());
		assertTrue(((String)appender.getLogBuffer().elementAt(0)).endsWith("message 1"));
		assertTrue(((String)appender.getLogBuffer().elementAt(1)).endsWith("message 2"));
		appender.close();
	}
	
	public void testClear() throws IOException
	{
		MemoryBufferAppender appender = new MemoryBufferAppender(2, false);
		appender.open();
		appender.doLog(null, "name", 10, Level.INFO, "message 1", null);
		assertEquals(1, appender.getLogSize());
		appender.clear();
		assertEquals(0, appender.getLogSize());		
		appender.close();
	}
}
