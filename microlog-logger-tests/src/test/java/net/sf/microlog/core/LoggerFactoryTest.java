/*
 * Copyright 2009 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.core;

import junit.framework.TestCase;

/**
 * Test class for the <code>LoggerFactory</code>
 * 
 * @author Johan Karlsson
 * 
 */
public class LoggerFactoryTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
		LoggerFactory.getLoggerRepository().reset();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		LoggerFactory.getLoggerRepository().reset();
	}

	public void testShutdown() {
		LoggerFactory.getLogger("net.sf.microlog.test.LoggerFactoryTest");
		LoggerFactory.getLogger("net.sf.microlog.test2.LoggerFactoryTest");
		Logger logger = LoggerFactory.getLogger("com.foo.Bar");

		logger.debug("Testing shutdown");

		LoggerFactory.shutdown();
	}

	public void testShutdownNoSetup() {
		LoggerFactory.shutdown();
	}

	public void testShutdownNoLogging() {
		LoggerFactory.getLogger("net.sf.microlog.test.LoggerFactoryTest");
		LoggerFactory.getLogger("net.sf.microlog.test2.LoggerFactoryTest");
		LoggerFactory.getLogger("com.foo.Bar");

		LoggerFactory.shutdown();
	}
}
