/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.microlog.core.format.command;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.format.command.DateFormatCommand;
import junit.framework.TestCase;

/**
 * Class to test the <code>TimeFormatCommand</code>.
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class DateFormatCommandTest extends TestCase {

	DateFormatCommand commandToTest;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		commandToTest = new DateFormatCommand();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testExecute() {
		String formattedString = commandToTest.execute(null,
				"net.sf.microlog.test", 12243, Level.DEBUG, "message", null);

		assertNotNull("The formatted String must not be null.", formattedString);
	}

	public void testToAbsoluteFormat() {
		String formattedString = commandToTest.toAbsoluteFormat(0);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The formatted String is not correct.", "00:00:00,000",
				formattedString);
	}

	public void testToDateFormat() {
		String formattedString = commandToTest.toDateFormat(0);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The formatted String is not correct.", "01 JAN 1970 00:00:00,000",
				formattedString);
	}
	
	public void testToISO8601Format(){
		String formattedString = commandToTest.toISO8601Format(0);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The formatted String is not correct.", "1970-01-01 00:00:00,000",
				formattedString);
	}
}
