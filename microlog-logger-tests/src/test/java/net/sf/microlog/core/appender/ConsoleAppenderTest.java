/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.microlog.core.appender;

import java.io.IOException;

import junit.framework.TestCase;
import net.sf.microlog.core.Appender;
import net.sf.microlog.core.Level;
import net.sf.microlog.core.appender.ConsoleAppender;

/**
 * Class to the <code>ConsoleAppender</code>.
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class ConsoleAppenderTest extends TestCase {

	private ConsoleAppender classUnderTest;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	public void setUp() throws Exception {
		classUnderTest = new ConsoleAppender();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	public void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for
	 * {@link net.sf.microlog.core.appender.ConsoleAppender#doLog(String, String, long, net.sf.microlog.core.Level, java.lang.Object, java.lang.Throwable)}.
	 */
	public void testDoLog() {
		classUnderTest.doLog("", "", 0, Level.DEBUG, "message", null);
		// TODO Check if the logging was done.
	}

	/**
	 * Test method for
	 * {@link net.sf.microlog.core.appender.ConsoleAppender#close()}.
	 */
	public void testCloseLog() {
		try {
			classUnderTest.close();
		} catch (IOException e) {
			fail("IOException was not expected.");
		}
		assertFalse("The log shall not be opened.", classUnderTest.isLogOpen());
	}

	/**
	 * Test method for {@link net.sf.microlog.core.appender.ConsoleAppender#open()}.
	 */
	public void testOpenLog() {
		try {
			classUnderTest.open();
		} catch (IOException e) {
			fail("IOException was not expected.");
		}
		assertTrue("The log shall be open.", classUnderTest.isLogOpen());
	}

	/**
	 * Test method for
	 * {@link net.sf.microlog.core.appender.ConsoleAppender#getLogSize()}.
	 */
	public void testGetLogSize() {
		assertEquals("", Appender.SIZE_UNDEFINED, classUnderTest.getLogSize());
	}

}
