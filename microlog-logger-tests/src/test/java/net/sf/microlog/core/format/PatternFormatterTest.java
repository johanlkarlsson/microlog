/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.microlog.core.format;

import junit.framework.TestCase;
import net.sf.microlog.core.Level;
import net.sf.microlog.core.format.PatternFormatter;

/**
 * Class to test the PatternFormatter class.
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class PatternFormatterTest extends TestCase {

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testDefaultFormat() {
		PatternFormatter formatter = new PatternFormatter();

		String formattedString = formatter.format(null, "a.b.c", 0,
				Level.TRACE, "message", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"0 c [TRACE] message ", formattedString);
	}

	public void testDefaultFormatNullLevel() {
		PatternFormatter formatter = new PatternFormatter();

		String formattedString = formatter.format(null, "a.b.c", 0,
				null, "Using a null level", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"0 c [     ] Using a null level ", formattedString);
	}

	public void testDefaultFormatNullMessage() {
		PatternFormatter formatter = new PatternFormatter();

		String formattedString = formatter.format(null, "a.b.c", 123,
				Level.FATAL, null, new Exception("Exception"));

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"123 c [FATAL]  java.lang.Exception: Exception", formattedString);
	}

	public void testDefaultFormatNullException() {
		PatternFormatter formatter = new PatternFormatter();

		String formattedString = formatter.format(null, "a.b.c", 561,
				Level.INFO, "message", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"561 c [INFO ] message ", formattedString);
	}
	
	public void testFormati(){
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%i");

		String formattedString = formatter.format(
				"client1", "net.sf.microlog.PatternFormatterTest", 1234,
				Level.FATAL, "Microlog is the best logger for Jave ME", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"client1", formattedString);
	}

	public void testFormatC() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%c");

		String formattedString = formatter.format(
				null, "net.sf.microlog.PatternFormatterTest", 1234,
				Level.FATAL, "Microlog is the best logger for Jave ME", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"PatternFormatterTest", formattedString);
	}
	
	public void testFormatCWithPrecisionSpecifier() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%c{1}");

		String formattedString = formatter.format(
				null, "net.sf.microlog.PatternFormatterTest", 1234,
				Level.FATAL, "Microlog is the best logger for Jave ME", null);
		System.out.println("Formatted String is "+formattedString);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"PatternFormatterTest", formattedString);
	}
	
	public void testFormatCWithPrecisionSpecifierThree() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%c{3}");

		String formattedString = formatter.format(
				null, "net.sf.microlog.PatternFormatterTest", 1234,
				Level.FATAL, "Microlog is the best logger for Jave ME", null);
		
		System.out.println("Formatted String is "+formattedString);
		
		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"sf.microlog.PatternFormatterTest", formattedString);
	}
	
	public void testFormatDate(){
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%d");
		
		String formattedString = formatter.format(
				null, "net.sf.microlog.PatternFormatterTest", 1234,
				Level.FATAL, "Microlog is the best logger for Jave ME", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		
		// TODO Check for the correct String
	}
	
	public void testFormatDateAbsolute(){
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%d{ABSOLUTE}");
		
		String formattedString = formatter.format(
				null, "net.sf.microlog.PatternFormatterTest", 1234,
				Level.FATAL, "Microlog is the best logger for Jave ME", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		
		// TODO Check for the correct String
	}
	
	public void testFormatDateDate(){
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%d{DATE}");
		
		String formattedString = formatter.format(
				null, "net.sf.microlog.PatternFormatterTest", 1234,
				Level.FATAL, "Microlog is the best logger for Jave ME", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		
		// TODO Check for the correct String
	}
	
	public void testFormatDateISO8601(){
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%d{ISO8601}");
		
		String formattedString = formatter.format(
				null, "net.sf.microlog.PatternFormatterTest", 1234,
				Level.FATAL, "Microlog is the best logger for Jave ME", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		
		// TODO Check for the correct String
	}

	public void testFormatM() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%m");

		String formattedString = formatter.format(null, "", 1234,
				Level.FATAL, "Microlog is the best logger for Jave ME", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"Microlog is the best logger for Jave ME", formattedString);
	}

	public void testFormatP() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%P");

		String formattedString = formatter.format(null, "", 1234,
				Level.TRACE, "Microlog is the best logger for Jave ME", new Exception(
								"Exception message"));

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				Level.TRACE_STRING, formattedString);
	}

	public void testFormatR() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%r");

		String formattedString = formatter.format(null, "", 100,
				Level.TRACE, "message", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.", "100",
				formattedString);
	}

	public void testFormatThread() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%t");

		String formattedString = formatter.format(null, "", 100,
				Level.TRACE, "message", new Exception("An important exception message"));

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.", Thread
				.currentThread().getName(), formattedString);
	}

	public void testFormatT() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%T");

		String formattedString = formatter.format(null, "", 100,
				Level.TRACE, "message", new Exception("An important exception message"));

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"java.lang.Exception: An important exception message",
				formattedString);
	}

	public void testFormatTNullMessage() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%T");

		String formattedString = formatter.format(null, "", 100,
				Level.TRACE, "message", null);

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.", "",
				formattedString);
	}

	public void testFormatPercent() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%%");

		String formattedString = formatter.format(null, "", 100,
				Level.TRACE, "message", new Exception("An important exception message"));

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.", "%",
				formattedString);
	}

	public void testFormatNothing() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("1234567890-Message");

		String formattedString = formatter.format(null, "", 100,
				Level.TRACE, "message", new Exception("An important exception message"));

		assertNotNull("The formatted String must not be null.", formattedString);
		assertEquals("The message is not correctly formatted.",
				"1234567890-Message", formattedString);
	}

	public void testFormatNullPattern() {
		PatternFormatter formatter = new PatternFormatter();
		try {
			formatter.setPattern(null);
			fail("An IllegalArgumentException was expected.");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}
	}

	public void testFormatUnrecognizedChar() {

		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%h");

		String formattedString = formatter.format(
				null, "net.sf.microlog.format.PatternFormatterTest", 0,
				Level.TRACE, "message", null);

		assertNotNull("The formatted String shall not be null.",
				formattedString);

		assertEquals("The message is not correctly formatted.", "",
				formattedString);
	}

	public void testFormatUnrecogniezCharEmbedded() {
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%h%m");

		String formattedString = formatter.format(
				null, "net.sf.microlog.format.PatternFormatterTest", 0,
				Level.TRACE, "message", null);

		assertNotNull("The formatted String shall not be null.",
				formattedString);
		assertEquals("The message is not correctly formatted.", "message",
				formattedString);
	}
	
	public void testExtractSpecifier(){
		PatternFormatter formatter = new PatternFormatter();
		
		String extractedSpecifier = formatter.extraxtSpecifier("%c{1}", 0);
		
		assertNotNull("The extracted specifier must not be null.", extractedSpecifier);
		assertEquals("Not the correct specifier", "1", extractedSpecifier);
	}
	
	public void testExtractSpecifierDate(){
		PatternFormatter formatter = new PatternFormatter();
		
		String extractedSpecifier = formatter.extraxtSpecifier("%d{DATE} %m", 0);
		
		assertNotNull("The extracted specifier must not be null.", extractedSpecifier);
		assertEquals("Not the correct specifier", "DATE", extractedSpecifier);
	}
	
}
