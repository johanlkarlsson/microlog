package net.sf.microlog.core;

import junit.framework.TestCase;

public class StringUtilTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testExtractPartialCategoryName() {
		String partialName = StringUtil.extractPartialClassName(
				"net.sf.microlog.test.SomeClassName", 1);

		assertNotNull("The partial name must never be null", partialName);
		assertEquals("The partial name is not correct", "SomeClassName",
				partialName);
	}

	public void testExtractPartialCategoryNameTwo() {
		String partialName = StringUtil.extractPartialClassName(
				"net.sf.microlog.test.SomeClassName", 2);

		assertNotNull("The partial name must never be null", partialName);
		assertEquals("The partial name is not correct", "test.SomeClassName",
				partialName);
	}

	public void testExtractPartialCategoryNameNoDots() {

		String partialName = StringUtil.extractPartialClassName(
				"SomeClassName", 1);

		assertNotNull("The partial name must never be null", partialName);
		assertEquals("The partial name is not correct", "SomeClassName",
				partialName);
	}

	public void testExtractPartialCategoryNameSpecifiedEqualsActual() {

		String partialName = StringUtil.extractPartialClassName(
				"net.sf.microlog.test.SomeClassName", 4);

		assertNotNull("The partial name must never be null", partialName);
		assertEquals("The partial name is not correct",
				"sf.microlog.test.SomeClassName", partialName);
	}

	public void testExtractPartialCategoryNameLessDotsThanSpecified() {

		String partialName = StringUtil.extractPartialClassName(
				"net.sf.microlog.test.SomeClassName", 5);

		assertNotNull("The partial name must never be null", partialName);
		assertEquals("The partial name is not correct",
				"net.sf.microlog.test.SomeClassName", partialName);
	}

}
