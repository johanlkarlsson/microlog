/*
 * Copyright 2009 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.microlog.core.config;

import junit.framework.TestCase;
import net.sf.microlog.core.Appender;
import net.sf.microlog.core.Formatter;
import net.sf.microlog.core.Level;
import net.sf.microlog.core.Logger;
import net.sf.microlog.core.LoggerFactory;
import net.sf.microlog.core.appender.ConsoleAppender;
import net.sf.microlog.core.appender.MemoryBufferAppender;
import net.sf.microlog.core.config.LoggerRepository;
import net.sf.microlog.core.config.PropertyConfigurator;
import net.sf.microlog.core.format.PatternFormatter;
import net.sf.microlog.core.format.SimpleFormatter;
import net.sf.microlog.midp.appender.DatagramAppender;
import net.sf.microlog.midp.appender.RecordStoreAppender;
import net.sf.microlog.midp.file.FileAppender;
import net.sf.microproperties.Properties;

/**
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 * 
 */
public class PropertyConfiguratorTest extends TestCase {

	private PropertyConfigurator configurator;
	private Properties properties;

	private Appender dummyAppender;
	private Formatter dummyFormatter; 

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		configurator = new PropertyConfigurator();
		properties = new Properties();
		dummyAppender = new DummyAppender();
		dummyFormatter = new DummyFormatter();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		LoggerFactory.getLoggerRepository().reset();
	}

	public void testConfigure() {

		properties.setProperty("microlog.level", "ERROR");
		properties.setProperty("microlog.appender",
				"net.sf.microlog.core.appender.ConsoleAppender");
		properties.setProperty("microlog.formatter",
				"net.sf.microlog.core.format.SimpleFormatter");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.ERROR, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());
		Appender appender = logger.getAppender(0);
		assertTrue("Not the correct appender.",
				appender instanceof ConsoleAppender);
		assertTrue("Not the correct formatter",
				appender.getFormatter() instanceof SimpleFormatter);
	}

	public void testConfigurePatternFormatter() {
		properties.setProperty("microlog.level", "ERROR");
		properties.setProperty("microlog.appender",
				"net.sf.microlog.core.appender.ConsoleAppender");
		properties.setProperty("microlog.formatter",
				"net.sf.microlog.core.format.PatternFormatter");
		properties.setProperty("microlog.formatter.PatternFormatter.pattern",
				"%c [%P] %m %T %%");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.ERROR, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());
		Appender appender = logger.getAppender(0);
		assertTrue("Not the correct appender.",
				appender instanceof ConsoleAppender);
		Formatter formatter = appender.getFormatter();
		assertTrue("Not the correct formatter",
				formatter instanceof PatternFormatter);
		PatternFormatter patternFormatter = (PatternFormatter) formatter;
		assertEquals("Not the correct pattern", "%c [%P] %m %T %%",
				patternFormatter.getPattern());
	}

	public void testConfigureRootLooger() {
		properties.setProperty("microlog.rootLogger", "FATAL, A");
		properties.setProperty("microlog.appender.A",
				"net.sf.microlog.core.appender.ConsoleAppender");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.FATAL, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());
		Appender appender = logger.getAppender(0);
		assertTrue("Not the correct appender.",
				appender instanceof ConsoleAppender);
	}

	public void testConfigureRootLoogerNoLevel() {

		properties.setProperty("microlog.rootLogger", "A");
		properties.setProperty("microlog.appender.A",
				"net.sf.microlog.core.appender.ConsoleAppender");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.DEBUG, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());
		Appender appender = logger.getAppender(0);
		assertTrue("Not the correct appender.",
				appender instanceof ConsoleAppender);
	}

	public void testConfigureRootLoggerNoLevelTwoAppenders() {
		properties.setProperty("microlog.rootLogger", "A1, A2");
		properties.setProperty("microlog.appender.A1",
				"net.sf.microlog.core.appender.ConsoleAppender");
		properties.setProperty("microlog.appender.A2",
				"net.sf.microlog.core.appender.MemoryBufferAppender");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.DEBUG, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 2, logger
				.getNumberOfAppenders());
		Appender appender1 = logger.getAppender(0);
		assertTrue("Not the correct appender.",
				appender1 instanceof ConsoleAppender);
		Appender appender2 = logger.getAppender(1);
		assertTrue("Not the correct appender.",
				appender2 instanceof MemoryBufferAppender);
	}

	public void testConfigureRootLoggerTwoAppenders() {
		properties.setProperty("microlog.rootLogger", "ERROR, A1, A2");
		properties.setProperty("microlog.appender.A1",
				"net.sf.microlog.core.appender.ConsoleAppender");
		properties.setProperty("microlog.appender.A2",
				"net.sf.microlog.core.appender.MemoryBufferAppender");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.ERROR, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 2, logger
				.getNumberOfAppenders());
		Appender appender1 = logger.getAppender(0);
		assertTrue("Not the correct appender.",
				appender1 instanceof ConsoleAppender);
		Appender appender2 = logger.getAppender(1);
		assertTrue("Not the correct appender.",
				appender2 instanceof MemoryBufferAppender);
	}

	public void testConfigureRootLoggerMissingAppender() {
		properties.setProperty("microlog.rootLogger", "WARN, A");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.WARN, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 0, logger
				.getNumberOfAppenders());
	}

	public void testConfigureRootLoggerIncorrectAppenderClass() {
		properties.setProperty("microlog.rootLogger", "FATAL, A");
		properties.setProperty("microlog.appender.A",
				"net.sf.microlog.appender.ConsoleAppender");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.FATAL, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 0, logger
				.getNumberOfAppenders());
	}

	public void testConfigureRootLoggerFormatter() {
		properties.setProperty("microlog.rootLogger", "FATAL, A");
		properties.setProperty("microlog.appender.A",
				"net.sf.microlog.core.appender.ConsoleAppender");
		properties.setProperty("microlog.appender.A.formatter",
				"net.sf.microlog.core.format.PatternFormatter");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.FATAL, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());
		Appender appender = logger.getAppender(0);
		assertTrue("Not the correct appender.",
				appender instanceof ConsoleAppender);
		assertTrue("Not the correct formatter",
				appender.getFormatter() instanceof PatternFormatter);
	}

	public void testConfigureRootLoggerFormatterProperties() {
		properties.setProperty("microlog.rootLogger", "FATAL, A");
		properties.setProperty("microlog.appender.A",
				"net.sf.microlog.core.appender.ConsoleAppender");
		properties.setProperty("microlog.appender.A.formatter",
				"net.sf.microlog.core.format.PatternFormatter");
		properties.setProperty("microlog.appender.A.formatter.pattern",
				"%c [%P] %m %T %%");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.FATAL, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());
		Appender appender = logger.getAppender(0);
		assertTrue("Not the correct appender.",
				appender instanceof ConsoleAppender);
		Formatter formatter = appender.getFormatter();
		assertTrue("Not the correct formatter",
				formatter instanceof PatternFormatter);
		PatternFormatter patternFormatter = (PatternFormatter) formatter;
		assertEquals("Not the correct pattern", "%c [%P] %m %T %%",
				patternFormatter.getPattern());
	}

	public void testConfigureRootLoggerAppenderProperties() {
		properties.setProperty("microlog.rootLogger", "FATAL, A");
		properties.setProperty("microlog.appender.A",
				"net.sf.microlog.midp.appender.DatagramAppender");
		properties.setProperty("microlog.appender.A.host", "192.168.0.1");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		assertNotNull("The logger shall not be null.", logger);
		assertEquals("Not the correct level", Level.FATAL, logger.getLevel());
		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());
		Appender appender = logger.getAppender(0);
		assertTrue("Not the correct appender.",
				appender instanceof DatagramAppender);
		DatagramAppender datagramAppender = (DatagramAppender) appender;
		assertEquals("Not the correct host", "192.168.0.1", datagramAppender
				.getHost());
	}

	public void testDoConfigureHierarchyLogLevels() {

		Logger logger = LoggerFactory.getLogger(PropertyConfigurator.class);

		properties.setProperty("microlog.rootLogger", "DEBUG");
		properties.setProperty("microlog.logger.net.sf.microlog", "FATAL");

		configurator.configure(properties);

		LoggerRepository repository = LoggerFactory.getLoggerRepository();
		assertEquals("Not the correct level", Level.FATAL, repository
				.getEffectiveLevel(logger));
	}

	public void testLoggingHierarchyConfiguration() {

		properties.setProperty("microlog.rootLogger", "TRACE, A1, A2");
		properties.setProperty("microlog.appender.A1",
				"net.sf.microlog.core.appender.ConsoleAppender");
		properties.setProperty(
				"microlog.logger.net.sf.microlog.midp.example.b", "INFO");
		properties.setProperty(
				"microlog.logger.net.sf.microlog.midp.example.b.c", "DEBUG");

		Logger fooLogger = LoggerFactory
				.getLogger("net.sf.microlog.midp.example.b.Foo");
		Logger barLogger = LoggerFactory
				.getLogger("net.sf.microlog.midp.example.b.c.Bar");

		assertNotNull("The logger must not be null", fooLogger);
		assertEquals("Not the right category name",
				"net.sf.microlog.midp.example.b.Foo", fooLogger.getName());

		assertNotNull("The logger must not be null", barLogger);
		assertEquals("Not the right category name",
				"net.sf.microlog.midp.example.b.c.Bar", barLogger.getName());
	}

	/**
	 * Test method for
	 * {@link net.sf.microlog.core.config.PropertyConfigurator#doConfigureAppender(Logger, java.lang.String, net.sf.microproperties.Properties)}
	 * .
	 */
	public void testDoConfigureAppender() {

		Logger logger = configurator.loggerRepository.getRootLogger();

		properties.setProperty("microlog.appender.A",
				"net.sf.microlog.core.appender.ConsoleAppender");
		properties.setProperty("microlog.appender.A.formatter",
				"net.sf.microlog.core.format.SimpleFormatter");

		configurator.doConfigureAppender(logger, "A", properties);

		assertTrue("Not the correct appender.",
				logger.getAppender(0) instanceof ConsoleAppender);
	}

	/**
	 * Test method for
	 * {@link net.sf.microlog.core.config.PropertyConfigurator#createAppender(java.lang.String, net.sf.microproperties.Properties)}
	 * .
	 */
	public void testCreateAppender() {

		properties.setProperty("microlog.appender.A",
				"net.sf.microlog.core.appender.ConsoleAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue(
				"The appender is not the right class",
				appender instanceof net.sf.microlog.core.appender.ConsoleAppender);
	}

	public void testCreateAppenderWithAliasConsoleAppender() {

		properties.setProperty("microlog.appender.A", "ConsoleAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue(
				"The appender is not the right class",
				appender instanceof net.sf.microlog.core.appender.ConsoleAppender);
	}

	public void testCreateAppenderWithAliasMemoryBufferAppender() {

		properties.setProperty("microlog.appender.A", "MemoryBufferAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue(
				"The appender is not the right class",
				appender instanceof net.sf.microlog.core.appender.MemoryBufferAppender);
	}

	public void testCreateAppenderWithAliasBluetoothSerialAppender() {

		properties
				.setProperty("microlog.appender.A", "BluetoothSerialAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue(
				"The appender is not the right class",
				appender instanceof net.sf.microlog.midp.bluetooth.BluetoothSerialAppender);
	}

	public void testCreateAppenderWithAliasDatagramAppender() {

		properties.setProperty("microlog.appender.A", "DatagramAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue(
				"The appender is not the right class",
				appender instanceof net.sf.microlog.midp.appender.DatagramAppender);
	}

	public void testCreateAppenderWithAliasHttpAppender() {

		properties.setProperty("microlog.appender.A", "HttpAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue("The appender is not the right class",
				appender instanceof net.sf.microlog.midp.appender.HttpAppender);
	}

	public void testCreateAppenderWithAliasFileAppender() {

		properties.setProperty("microlog.appender.A", "FileAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue("The appender is not the right class",
				appender instanceof net.sf.microlog.midp.file.FileAppender);
	}

	public void testCreateAppenderWithAliasFormAppender() {

		properties.setProperty("microlog.appender.A", "FormAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue("The appender is not the right class",
				appender instanceof net.sf.microlog.midp.appender.FormAppender);
	}

	public void testCreateAppenderWithAliasMMSBufferAppender() {

		properties.setProperty("microlog.appender.A", "MMSBufferAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue("The appender is not the right class",
				appender instanceof net.sf.microlog.midp.wma.MMSBufferAppender);
	}

	public void testCreateAppenderWithAliasRecordStoreAppender() {

		properties.setProperty("microlog.appender.A", "RecordStoreAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue(
				"The appender is not the right class",
				appender instanceof net.sf.microlog.midp.appender.RecordStoreAppender);
	}

	public void testCreateAppenderWithAliasSerialAppender() {

		properties.setProperty("microlog.appender.A", "SerialAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue(
				"The appender is not the right class",
				appender instanceof net.sf.microlog.midp.appender.SerialAppender);
	}

	public void testCreateAppenderWithAliasSMSBufferAppender() {

		properties.setProperty("microlog.appender.A", "SMSBufferAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue("The appender is not the right class",
				appender instanceof net.sf.microlog.midp.wma.SMSBufferAppender);
	}

	public void testCreateAppenderWithAliasSocketAppender() {

		properties.setProperty("microlog.appender.A", "SocketAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue(
				"The appender is not the right class",
				appender instanceof net.sf.microlog.midp.appender.SocketAppender);
	}

	public void testCreateAppenderWithAliasSyslogAppender() {

		properties.setProperty("microlog.appender.A", "SyslogAppender");

		Appender appender = configurator.createAppender("A", properties);

		assertNotNull("The appender must not be null.", appender);
		assertTrue(
				"The appender is not the right class",
				appender instanceof net.sf.microlog.midp.appender.SyslogAppender);
	}

	/**
	 * Test method for
	 * {@link net.sf.microlog.core.config.PropertyConfigurator#createAppender(java.lang.String, net.sf.microproperties.Properties)}
	 * .
	 */
	public void testCreateAppenderNotExisting() {

		Appender appender = configurator.createAppender("A", properties);

		assertNull("The appender must be null.", appender);

	}

	/**
	 * Test method for
	 * {@link net.sf.microlog.core.config.PropertyConfigurator#createFormatter(java.lang.String, net.sf.microproperties.Properties)}
	 * .
	 */
	public void testCreateFormatter() {

		properties.setProperty("microlog.appender.A.formatter",
				"net.sf.microlog.core.format.SimpleFormatter");

		Formatter formatter = configurator.createFormatter("A", properties);

		assertNotNull("The formatter must not be null.", formatter);
		assertTrue(
				"The formatter is not the right class",
				formatter instanceof net.sf.microlog.core.format.SimpleFormatter);
	}

	public void testCreateFormatterAliasSimpleFormatter() {

		properties.setProperty("microlog.appender.A.formatter",
				"SimpleFormatter");

		Formatter formatter = configurator.createFormatter("A", properties);

		assertNotNull("The formatter must not be null.", formatter);
		assertTrue(
				"The formatter is not the right class",
				formatter instanceof net.sf.microlog.core.format.SimpleFormatter);
	}

	public void testCreateFormatterAliasPatternFormatter() {

		properties.setProperty("microlog.appender.A.formatter",
				"PatternFormatter");

		Formatter formatter = configurator.createFormatter("A", properties);

		assertNotNull("The formatter must not be null.", formatter);
		assertTrue(
				"The formatter is not the right class",
				formatter instanceof net.sf.microlog.core.format.PatternFormatter);
	}

	public void testCreateFormatterNotExisting() {

		Formatter formatter = configurator.createFormatter("A", properties);

		assertNull("The formatter must be null.", formatter);
	}

	/**
	 * Test method for
	 * {@link net.sf.microlog.core.config.PropertyConfigurator#setAppenderSpecificProperties(java.lang.String, net.sf.microproperties.Properties, net.sf.microlog.core.Appender)}
	 * .
	 */
	public void testSetAppenderSpecificProperties() {

		properties.setProperty("microlog.appender.A",
				"net.sf.microlog.midp.appender.DatgramAppender");
		Appender appender = new DatagramAppender();

		configurator.setAppenderSpecificProperties("A", properties, appender);

		// TODO Check that the properties was set correctly
	}

	public void testConfigureAppender() {
		properties.setProperty("microlog.appender",
				"net.sf.microlog.core.appender.ConsoleAppender");

		configurator.configureAppender(properties);

		Logger logger = LoggerFactory.getLogger();

		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());
		assertTrue(
				"Not the right class for the Appender",
				logger.getAppender(0) instanceof net.sf.microlog.core.appender.ConsoleAppender);
	}

	public void testConfigureAppenderTwoAppenders() {
		properties
				.setProperty(
						"microlog.appender",
						"net.sf.microlog.core.appender.ConsoleAppender;net.sf.microlog.core.appender.MemoryBufferAppender");

		configurator.configureAppender(properties);

		Logger logger = LoggerFactory.getLogger();

		assertEquals("Not the correct number of appenders.", 2, logger
				.getNumberOfAppenders());

		assertTrue(
				"Not the right class for the Appender",
				logger.getAppender(0) instanceof net.sf.microlog.core.appender.ConsoleAppender);
		assertTrue(
				"Not the right class for the Appender",
				logger.getAppender(1) instanceof net.sf.microlog.core.appender.MemoryBufferAppender);
	}

	public void testConfigureAppenderWithAlias() {
		properties.setProperty("microlog.appender", "ConsoleAppender");

		configurator.configureAppender(properties);

		Logger logger = LoggerFactory.getLogger();

		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());
		assertTrue(
				"Not the right class for the Appender",
				logger.getAppender(0) instanceof net.sf.microlog.core.appender.ConsoleAppender);
	}

	public void testConfigureAppenderWithAliasTwoAppenders() {
		properties.setProperty("microlog.appender",
				"ConsoleAppender;MemoryBufferAppender");

		configurator.configureAppender(properties);

		Logger logger = LoggerFactory.getLogger();

		assertEquals("Not the correct number of appenders.", 2, logger
				.getNumberOfAppenders());

		assertTrue(
				"Not the right class for the Appender",
				logger.getAppender(0) instanceof net.sf.microlog.core.appender.ConsoleAppender);
		assertTrue(
				"Not the right class for the Appender",
				logger.getAppender(1) instanceof net.sf.microlog.core.appender.MemoryBufferAppender);
	}

	public void testSetRecordStoreName() {

		properties.setProperty("microlog.appender", "RecordStoreAppender");
		properties.setProperty(
				"microlog.appender.RecordStoreAppender.recordStoreName",
				"MyLogRMS");

		configurator.configureAppender(properties);

		// Check that everything is working ok
		Logger logger = LoggerFactory.getLogger();

		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());

		Appender appender = logger.getAppender(0);

		assertTrue(
				"Not the right class for the Appender",
				appender instanceof net.sf.microlog.midp.appender.RecordStoreAppender);
		RecordStoreAppender recordStoreAppender = (RecordStoreAppender) appender;

		assertEquals("Not the correct RMS name", "MyLogRMS",
				recordStoreAppender.getRecordStoreName());

	}

	public void testSetMaxRecordLogEntries() {
		properties.setProperty("microlog.appender", "RecordStoreAppender");
		properties.setProperty(
				"microlog.appender.RecordStoreAppender.maxRecordStoreEntries",
				"64");

		configurator.configureAppender(properties);

		// Check that everything is working ok
		Logger logger = LoggerFactory.getLogger();

		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());

		Appender appender = logger.getAppender(0);

		assertTrue(
				"Not the right class for the Appender",
				appender instanceof net.sf.microlog.midp.appender.RecordStoreAppender);
		RecordStoreAppender recordStoreAppender = (RecordStoreAppender) appender;

		assertEquals("Not the correct RMS number of maxLogEntries", 64,
				recordStoreAppender.getMaxRecordEntries());
	}

	public void testSetFileName() {
		properties.setProperty("microlog.appender", "FileAppender");
		properties.setProperty("microlog.appender.FileAppender.filename",
				"MicroLogFile.txt");

		configurator.configureAppender(properties);

		// Check that everything is working ok
		Logger logger = LoggerFactory.getLogger();

		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());

		Appender appender = logger.getAppender(0);

		assertTrue("Not the right class for the Appender",
				appender instanceof net.sf.microlog.midp.file.FileAppender);
		FileAppender fileAppender = (FileAppender) appender;

		assertEquals("Not the correct filename", "MicroLogFile.txt",
				fileAppender.getFileName());
	}

	public void testSetPatternFormatter() {
		properties.setProperty("microlog.appender", "ConsoleAppender");
		properties.setProperty("microlog.formatter", "PatternFormatter");
		properties.setProperty("microlog.formatter.PatternFormatter.pattern",
				"%c %m %t");

		configurator.configure(properties);

		// Check that everything is working ok
		Logger logger = LoggerFactory.getLogger();

		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());

		Appender appender = logger.getAppender(0);

		Formatter formatter = appender.getFormatter();

		System.out.println("formatter is " + formatter.getClass().getName());
		assertTrue(
				"Not the correct type of Formatter, expected PatternFormatter",
				formatter instanceof PatternFormatter);

		PatternFormatter patternFormatter = (PatternFormatter) formatter;

		assertEquals("Not the correct pattern.", "%c %m %t", patternFormatter
				.getPattern());
	}

	public void testSimpleConfiguration() {
		properties.setProperty("microlog.rootLogger", "ERROR, A1");
		properties.setProperty("microlog.appender.A1", "ConsoleAppender");
		properties.setProperty("microlog.appender.A1.formatter",
				"PatternFormatter");
		properties.setProperty("microlog.appender.A1.formatter.pattern",
				"%c{1} [%P] %m %T");
		properties.setProperty("microlog.logger.com.jayway.midlet.techtip",
				"DEBUG");

		configurator.configure(properties);

		// Check that everything is working ok
		Logger logger = LoggerFactory.getLogger();

		assertEquals("Not the correct number of appenders.", 1, logger
				.getNumberOfAppenders());

		Appender appender = logger.getAppender(0);

		assertTrue("Not the correct appender type",
				appender instanceof ConsoleAppender);

		Formatter formatter = appender.getFormatter();

		assertTrue(
				"Not the correct type of Formatter, expected PatternFormatter",
				formatter instanceof PatternFormatter);

		PatternFormatter patternFormatter = (PatternFormatter) formatter;
		assertEquals("Not the correct pattern", "%c{1} [%P] %m %T",
				patternFormatter.getPattern());
	}

	public void testConfigureNoFormatter() {

		properties.setProperty("microlog.level", "DEBUG");
		properties.setProperty("microlog.appender", "FileAppender");
		properties.setProperty("microlog.appender.FileAppender.filename",
				"C:/My Documents/Log.txt");

		configurator.configure(properties);

		Logger logger = LoggerFactory.getLogger();

		Appender appender = logger.getAppender(0);

		assertTrue("Not the correct appender type",
				appender instanceof FileAppender);

		FileAppender fileAppender = (FileAppender) appender;

		assertEquals("Not the correct filename", "C:/My Documents/Log.txt",
				fileAppender.getFileName());
	}

	public void testAddAppenderAlias() {

		PropertyConfigurator.addAppenderAlias(dummyAppender);

		Object name = PropertyConfigurator.appenderMap.get("DummyAppender");

		assertNotNull("Did not find the appender via alias", name);
		assertTrue("It must return a String object", name instanceof String);
		String fullyQualifiedClassName = (String) name;
		assertEquals("Not the correct fully qualified classname",
				"net.sf.microlog.core.config.DummyAppender", fullyQualifiedClassName);
	}

	public void testAddFormatterAlias() {
		PropertyConfigurator.addFormatterAlias(dummyFormatter);
		
		Object name = PropertyConfigurator.formatterMap.get("DummyFormatter");
		
		assertNotNull("Did not find formatter alias", name);
		assertTrue("It must return a String object", name instanceof String);
		String fullyQualifiedClassName = (String) name;
		assertEquals("Not the correct fully qualified classname",
				"net.sf.microlog.core.config.DummyFormatter", fullyQualifiedClassName);
	}

}
