/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.core.config;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.Logger;
import net.sf.microlog.core.config.DefaultLoggerRepository;
import junit.framework.TestCase;



public class LoggerRepositoryTest extends TestCase {

	private DefaultLoggerRepository loggerRepository = DefaultLoggerRepository.getInstance();
	
	protected void setUp() throws Exception {
		loggerRepository.reset();
	}

	protected void tearDown() throws Exception {
		loggerRepository.reset();
	}

	public void testGetLogger() {
		Logger logger = loggerRepository.getLogger("a.b.c.D");
		assertNotNull("The logger must not be null.", logger);
		assertEquals("Not the correct name of the logger", "a.b.c.D", logger
				.getName());
		assertEquals("Not the right number of loggers.", 1, loggerRepository
				.numberOfLeafNodes());
	}

	public void testGetLoggerByClassName() {
		Logger logger = loggerRepository.getLogger(LoggerRepositoryTest.class
				.getName());

		assertNotNull("The logger must not be null.", logger);
		assertEquals("Not the correct name of the logger",
				LoggerRepositoryTest.class.getName(), logger.getName());
		assertEquals("Not the right number of loggers.", 1, loggerRepository
				.numberOfLeafNodes());
	}

	public void testGetLoggerTwice() {
		Logger logger1 = loggerRepository
				.getLogger("net.sf.microlog.test.TestA");
		Logger logger2 = loggerRepository
				.getLogger("net.sf.microlog.test.TestA");
		assertSame("It must be the same instance", logger1, logger2);
		assertEquals("Not the right number of loggers.", 1, loggerRepository
				.numberOfLeafNodes());
	}

	public void testAddLogger() {
		Logger logger = new Logger("net.sf.microlog.test.Test");

		loggerRepository.addLogger(logger);

		assertTrue("Logger does not seems to be added.", loggerRepository
				.contains(logger.getName()));
	}

	public void testAddLoggerSamePackage() {
		Logger logger1 = new Logger("net.sf.microlog.test.Test");
		Logger logger2 = new Logger("net.sf.microlog.test.TestIt");

		loggerRepository.addLogger(logger1);
		assertEquals("Not the right number of loggers.", 1, loggerRepository
				.numberOfLeafNodes());
		assertTrue("LoggerTree is missing a logger instance.", loggerRepository
				.contains(logger1.getName()));

		loggerRepository.addLogger(logger2);
		assertEquals("Not the right number of loggers.", 2, loggerRepository
				.numberOfLeafNodes());
		assertTrue("LoggerTree is missing a logger instance.", loggerRepository
				.contains(logger2.getName()));
	}

	public void testAddLoggerDifferentPackages() {
		Logger logger1 = new Logger("net.sf.microlog.test.Test");
		Logger logger2 = new Logger("com.jayway.test.TestIt");

		loggerRepository.addLogger(logger1);
		assertEquals("Not the right number of loggers.", 1, loggerRepository
				.numberOfLeafNodes());
		assertTrue("LoggerTree is missing a logger instance.", loggerRepository
				.contains(logger1.getName()));

		loggerRepository.addLogger(logger2);
		assertEquals("Not the right number of loggers.", 2, loggerRepository
				.numberOfLeafNodes());
		assertTrue("LoggerTree is missing a logger instance.", loggerRepository
				.contains(logger2.getName()));
	}

	public void testAddLoggerDifferentPackagesButSameStart() {
		Logger logger1 = new Logger("net.sf.microlog.test.Test");
		Logger logger2 = new Logger("net.sf.microlog.testit.TestIt");

		loggerRepository.addLogger(logger1);
		assertEquals("Not the right number of loggers.", 1, loggerRepository
				.numberOfLeafNodes());
		assertTrue("LoggerTree is missing a logger instance.", loggerRepository
				.contains(logger1.getName()));

		loggerRepository.addLogger(logger2);
		assertEquals("Not the right number of loggers.", 2, loggerRepository
				.numberOfLeafNodes());
		assertTrue("LoggerTree is missing a logger instance.", loggerRepository
				.contains(logger2.getName()));
	}
	
	public void testSetLevel(){
		Logger logger = new Logger("net.sf.microlog.test.Test");
		loggerRepository.addLogger(logger);
		
		loggerRepository.setLevel("net.sf.microlog.test", Level.ERROR);
		
		Level level = loggerRepository.getEffectiveLevel(logger);
		assertNotNull("The level must not be null.", level);
		assertEquals("Not the correct level", Level.ERROR, level);
	}
	
	public void testSetLevelOnLeaf(){
		Logger logger = new Logger("net.sf.microlog.test.Test");
		loggerRepository.addLogger(logger);
		
		loggerRepository.setLevel("net.sf.microlog.test.Test", Level.ERROR);
		
		Level level = loggerRepository.getEffectiveLevel(logger);
		assertNotNull("The level must not be null.", level);
		assertEquals("Not the correct level", Level.ERROR, level);
	}
	
	public void testSetLevelNotExistingPath(){
		Logger logger = new Logger("net.sf.microlog.test.Test");
		loggerRepository.addLogger(logger);
		
		loggerRepository.setLevel("com.company", Level.ERROR);
		
		Level level = loggerRepository.getEffectiveLevel(logger);
		assertNotNull("The level must not be null.", level);
		assertEquals("Not the correct level", Level.DEBUG, level);
	}

	public void testGetEffectiveLevel() {
		Logger logger = new Logger("net.sf.microlog.test.Test");
		loggerRepository.addLogger(logger);

		Level level = loggerRepository.getEffectiveLevel(logger);

		assertNotNull("The level must never be null.", level);
		assertEquals("The level must be the default level.", Level.DEBUG, level);
	}

	public void testGetEffectiveLevelLoggerLevelSet() {
		Logger logger = new Logger("net.sf.microlog.test.Test");
		logger.setLevel(Level.WARN);
		loggerRepository.addLogger(logger);

		Level level = loggerRepository.getEffectiveLevel(logger);

		assertNotNull("The level must never be null.", level);
		assertEquals("The level must be the default level.", Level.WARN, level);
	}

	public void testGetEffectiveLevelTwoLoggers() {
		Logger logger1 = new Logger("net.sf.microlog.test.Test");
		Logger logger2 = new Logger("net.sf.microlog.testit.TestIt");
		loggerRepository.addLogger(logger1);
		loggerRepository.addLogger(logger2);

		Level level1 = loggerRepository.getEffectiveLevel(logger1);
		assertNotNull("The level must never be null.", level1);
		assertEquals("The level must be the default level.", Level.DEBUG, level1);
		
		Level level2 = loggerRepository.getEffectiveLevel(logger2);
		assertNotNull("The level must never be null.", level2);
		assertEquals("The level must be the default level.", Level.DEBUG, level2);
	}
}
