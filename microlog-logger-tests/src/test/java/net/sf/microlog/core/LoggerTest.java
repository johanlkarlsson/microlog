/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.core;

import java.util.Vector;

import junit.framework.TestCase;
import net.sf.microlog.core.appender.ConsoleAppender;
import net.sf.microlog.core.appender.MemoryBufferAppender;
import net.sf.microlog.core.config.DefaultLoggerRepository;

/**
 * This class is used for testing the Logger class.
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class LoggerTest extends TestCase {

	private Logger log;

	protected void setUp() throws Exception {
		log = LoggerFactory.getLogger();
		assertNotNull("log must not be null", log);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		DefaultLoggerRepository.getInstance().reset();
	}

	public void getNamedInstanceWithNull() {
		try {
			LoggerFactory.getLogger((String) null);
			fail("An exception was expected.");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}
	}

	public void getClassInstanceWithNull() {
		try {
			LoggerFactory.getLogger((Class) null);
			fail("An exception was expected.");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}
	}

	public void testGetSameInstance() {
		Logger log1 = LoggerFactory.getLogger();
		Logger log2 = LoggerFactory.getLogger();

		assertNotNull("The log1 must not be null.", log1);
		assertNotNull("The log2 must not be null.", log2);
		assertSame("It sahll be the same instance", log1, log2);
	}

	public void testGetSameInstanceWithName() {
		Logger log1 = LoggerFactory.getLogger("net.sf.microlog.LoggerTest");
		Logger log2 = LoggerFactory.getLogger("net.sf.microlog.LoggerTest");
		assertNotNull("The log1 must not be null.", log1);
		assertNotNull("The log2 must not be null.", log2);
		assertSame("It sahll be the same instance", log1, log2);
	}

	public void testGetSameInstanceWithClass() {
		Logger log1 = LoggerFactory.getLogger(LoggerTest.class);
		Logger log2 = LoggerFactory.getLogger(LoggerTest.class);
		assertNotNull("The log1 must not be null.", log1);
		assertNotNull("The log2 must not be null.", log2);
		assertSame("It sahll be the same instance", log1, log2);
	}

	public void testGetSameInstanceWithNullString() {
		try {
			LoggerFactory.getLogger((String) null);
			fail("An Exception was expected.");
		} catch (IllegalArgumentException e) {
			// This is expected
			assertTrue(true);
		}

	}

	public void testGetSameInstanceWithNullClass() {
		try {
			LoggerFactory.getLogger((Class) null);
			fail("An Exception was expected.");
		} catch (IllegalArgumentException e) {
			// This is expected
			assertTrue(true);
		}

	}

	public void testIsTraceEnabled() {
		Logger log = LoggerFactory.getLogger();
		log.setLevel(Level.TRACE);

		assertTrue("Trace level shall be enabled.", log.isTraceEnabled());
	}

	public void testIsTraceEnabledDebug() {
		Logger log = LoggerFactory.getLogger();
		log.setLevel(Level.TRACE);

		assertTrue("Debug level shall be enabled.", log.isDebugEnabled());
	}

	public void testIsDebugEnabled() {
		Logger log = LoggerFactory.getLogger();
		log.setLevel(Level.DEBUG);

		assertTrue("Debug level shall be enabled.", log.isDebugEnabled());
	}

	public void testIsDebugEnabledTrace() {
		Logger log = LoggerFactory.getLogger();
		log.setLevel(Level.DEBUG);

		assertFalse("Trace level shall not be enabled.", log.isTraceEnabled());
	}

	public void testIsDebugEnabledInfo() {
		Logger log = LoggerFactory.getLogger();
		log.setLevel(Level.DEBUG);

		assertTrue("Info level shall be enabled.", log.isInfoEnabled());
	}

	public void testIsInfoEnabled() {
		Logger log = LoggerFactory.getLogger();
		log.setLevel(Level.INFO);

		assertTrue("Info level shall be enabled.", log.isInfoEnabled());
	}

	public void testIsInfoEnabledDebug() {
		Logger log = LoggerFactory.getLogger();
		log.setLevel(Level.INFO);

		assertFalse("Debug level shall not be enabled.", log.isDebugEnabled());
	}

	public void testIsWarnEnabledInfo() {
		Logger log = LoggerFactory.getLogger();
		log.setLevel(Level.WARN);

		assertFalse("Info level shall not be enabled.", log.isInfoEnabled());
	}

	public void testAddAppender() {

		Appender appender = new ConsoleAppender();
		log.addAppender(appender);

		Appender fetchedAppender = log.getAppender(0);

		assertNotNull("The fetched appender shall not be null.",
				fetchedAppender);
		assertSame("The fetched appender is not the correct one.", appender,
				fetchedAppender);
		assertEquals("Not the correct number of appenders", 1, log
				.getNumberOfAppenders());

		log.removeAppender(appender);
	}

	public void testAddAppenders() {
		Logger log = LoggerFactory.getLogger();
		Appender appender1 = new ConsoleAppender();
		log.addAppender(appender1);
		Appender appender2 = new ConsoleAppender();
		log.addAppender(appender2);

		Appender fetchedAppender = log.getAppender(0);

		assertNotNull("The 1st fetched appender shall not be null.",
				fetchedAppender);
		assertSame("The 1st fetched appender is not the correct one.",
				appender1, fetchedAppender);

		fetchedAppender = log.getAppender(1);

		assertNotNull("The 2nd fetched appender shall not be null.",
				fetchedAppender);
		assertSame("The 2nd fetched appender is not the correct one.",
				appender2, fetchedAppender);

		assertEquals("Not the correct number of appenders", 2, log
				.getNumberOfAppenders());

		log.removeAppender(appender1);
		log.removeAppender(appender2);
	}

	public void testAddAppenderNull() {

		Logger log = LoggerFactory.getLogger();

		try {
			log.addAppender(null);
			fail("An exception was expected");
		} catch (IllegalArgumentException e) {
			// This is expected
			assertTrue(true);
		}

	}

	public void testAddAppenderTwice() {

		Logger log = LoggerFactory.getLogger();
		Appender appender = new ConsoleAppender();

		log.addAppender(appender);
		log.addAppender(appender);

		Appender fetchedAppender = log.getAppender(0);

		assertNotNull("The fetched appender shall not be null.",
				fetchedAppender);
		assertSame("The fetched appender is not the correct one.", appender,
				fetchedAppender);
		assertEquals("Not the correct number of appenders", 1, log
				.getNumberOfAppenders());

		log.removeAllAppenders();
	}

	public void testRemoveAppender() {

		Logger log = LoggerFactory.getLogger();
		Appender appender = new ConsoleAppender();
		log.addAppender(appender);

		Appender fetchedAppender = log.getAppender(0);

		assertNotNull("The fetched appender shall not be null.",
				fetchedAppender);
		assertSame("The fetched appender is not the correct one.", appender,
				fetchedAppender);
		assertEquals("Not the correct number of appenders", 1, log
				.getNumberOfAppenders());

		log.removeAppender(appender);
	}

	public void testRemoveAppenderNull() {

		Logger log = LoggerFactory.getLogger();

		try {
			log.removeAppender(null);
			fail("An exception was expected");
		} catch (IllegalArgumentException e) {
			// This is expected
			assertTrue(true);
		}
	}

	public void testRemoveAllAppendersNoAppenders() {

		log.removeAllAppenders();

		assertEquals("Not the correct number of appenders", 0, log
				.getNumberOfAppenders());
	}

	public void testSetLogLevelNull() {

		try {
			log.setLevel(null);
			fail("This was not expected.");
		} catch (IllegalArgumentException e) {
			// This is expected
			assertTrue(true);
		}
	}

	public void testGetLoggerName() {

		log = LoggerFactory.getLogger("net.sf.microlog.LoggerTest");

		String loggerName = log.getName();

		assertNotNull("The name must not be null", loggerName);
		assertEquals("Not the correct name", "net.sf.microlog.LoggerTest",
				loggerName);
	}

	public void testGetEffectiveLevel() {
		log = LoggerFactory.getLogger("net.sf.microlog.LoggerTest");
		log.setLevel(Level.ERROR);

		Level level = log.getEffectiveLevel();

		assertNotNull("The level must never be null.", level);
		assertEquals("Not the correct level", Level.ERROR, level);

	}

	public void testGetEffectiveLevelNoName() {
		log = LoggerFactory.getLogger();
		log.setLevel(Level.FATAL);

		Level level = log.getEffectiveLevel();

		assertNotNull("The level must never be null.", level);
		assertEquals("Not the correct level", Level.FATAL, level);
	}

	public void testGetEffectiveLevelNullLevel() {
		log = LoggerFactory.getLogger("net.sf.microlog.LoggerTest");

		Level level = log.getEffectiveLevel();

		assertNotNull("The level must never be null.", level);
		assertEquals("Not the correct level", Level.DEBUG, level);
	}

	// TODO add tests for the log(..) method(s)

	public void testLog() {
		log = LoggerFactory.getLogger();
		log.setLevel(Level.DEBUG);
		MemoryBufferAppender appender = new MemoryBufferAppender();
		log.addAppender(appender);

		log.debug("A logging message");

		Vector logBuffer = appender.getLogBuffer();
		assertEquals("Not the correct number of loggings ", 1, appender
				.getLogSize());
		assertEquals("Not the correct logging String",
				"0:[DEBUG]-A logging message", logBuffer.elementAt(0));
	}

	public void testLogNamedLogger() {
		log.setLevel(Level.DEBUG);
		MemoryBufferAppender appender = new MemoryBufferAppender();
		log.addAppender(appender);

		log.debug("A logging message");

		Vector logBuffer = appender.getLogBuffer();
		assertEquals("Not the correct number of loggings ", 1, appender
				.getLogSize());
		assertEquals("Not the correct logging String",
				"0:[DEBUG]-A logging message", logBuffer.elementAt(0));
	}

	public void testLogNoAppendersAdded() {
		log = LoggerFactory.getLogger();
		log.setLevel(Level.DEBUG);

		log.debug("A test logging message");

		assertEquals("Not the correct number of appenders", 1, log
				.getNumberOfAppenders());
		Appender appender = log.getAppender(0);
		assertTrue(
				"Not the correct appender",
				appender instanceof net.sf.microlog.core.appender.ConsoleAppender);
		Formatter formatter = appender.getFormatter();
		assertTrue(
				"Not the correct formatter",
				formatter instanceof net.sf.microlog.core.format.SimpleFormatter);
	}

}