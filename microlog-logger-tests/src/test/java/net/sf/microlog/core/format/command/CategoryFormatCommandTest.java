/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.core.format.command;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.format.command.CategoryFormatCommand;
import junit.framework.TestCase;

/**
 * Class to test the <code>CategoryFormatCommand</code>.
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class CategoryFormatCommandTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testExecute() {
		CategoryFormatCommand command = new CategoryFormatCommand();
		assertEquals("Not the correct string",
				"CategoryFormatCommandTest", command.execute(
						null, "net.sf.microlog.CategoryFormatCommandTest", 0,
						Level.DEBUG, "message", null));
	}

	public void testExecuteNullMessage() {
		CategoryFormatCommand command = new CategoryFormatCommand();
		assertEquals("It should be the same String", "", command.execute(null,
				null, 0, Level.DEBUG, "", new Exception("exception")));
	}
	
	public void testExecuteWithSpecifier() {
		CategoryFormatCommand command = new CategoryFormatCommand();
		command.init("1");
		
		assertEquals("It is not the correct string",
				"CategoryFormatCommandTest", command.execute(
						null, "net.sf.microlog.CategoryFormatCommandTest", 0,
						Level.DEBUG, "message", null));
	}

	public void testInit() {
		CategoryFormatCommand command = new CategoryFormatCommand();

		try {
			command.init("2");

			assertEquals("The precision specifier is not correctly parsed.", 2,
					command.getPrecisionSpecifier());
		} catch (NumberFormatException e) {
			fail("The init method must not throw a NumberFormatException");
		}
	}

	public void testInitNoNumber() {
		CategoryFormatCommand command = new CategoryFormatCommand();

		try {
			command.init("NotANumber");

			assertEquals("The precision specifier is not correctly parsed.", CategoryFormatCommand.DEFAULT_PRECISION_SPECIFIER,
					command.getPrecisionSpecifier());
		} catch (NumberFormatException e) {
			fail("The init method must not throw a NumberFormatException");
		}
	}

	}
