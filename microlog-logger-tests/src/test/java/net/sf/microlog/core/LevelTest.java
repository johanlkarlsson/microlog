/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.core;

import net.sf.microlog.core.Level;
import junit.framework.TestCase;

/**
 * This class is used for testing the Level class.
 *s
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class LevelTest extends TestCase {

	public void testLevelStringFATAL() {
		assertEquals("Not the correct level String", "FATAL", Level.FATAL
				.toString());
	}

	public void testLevelStringERROR() {
		assertEquals("Not the correct level String", "ERROR", Level.ERROR
				.toString());
	}

	public void testLevelStringWARN() {
		assertEquals("Not the correct level String", "WARN", Level.WARN
				.toString());
	}

	public void testLevelStringINFO() {
		assertEquals("Not the correct level String", "INFO", Level.INFO
				.toString());
	}

	public void testLevelStringDEBUG() {
		assertEquals("Not the correct level String", "DEBUG", Level.DEBUG
				.toString());
	}

	public void testLevelStringTRACE() {
		assertEquals("Not the correct level String", "TRACE", Level.TRACE
				.toString());
	}
	
	public void testEqualsFatalLevel(){
		assertTrue("Equals should be true", Level.FATAL.equals(Level.FATAL));
	}
	
	public void testEqualsErrorLevel(){
		assertTrue("Equals should be true", Level.ERROR.equals(Level.ERROR));
	}
	
	public void testEqualsWarnLevel(){
		assertTrue("Equals should be true", Level.WARN.equals(Level.WARN));
	}
	
	public void testEqualsInfoLevel(){
		assertTrue("Equals should be true", Level.INFO.equals(Level.INFO));
	}
	
	public void testEqualsDebugLevel(){
		assertTrue("Equals should be true", Level.DEBUG.equals(Level.DEBUG));
	}
	
	public void testEqualsTraceLevel(){
		assertTrue("Equals should be true", Level.TRACE.equals(Level.TRACE));
	}
	
	public void testEqualsNotTrue(){
		assertFalse("Equals shall not be true", Level.DEBUG.equals(Level.WARN));
	}
	
	public void testEqualsNull(){
		assertFalse("Equals shall not be true", Level.DEBUG.equals(null));
	}
	
	public void testEqualsNotLevelObject(){
		assertFalse("Equals shall not be true", Level.DEBUG.equals(new String("Test")));
	}
	
	public void testToIntFatal(){
		assertEquals("Not the correct value", 16, Level.FATAL.toInt());
	}
	
	public void testToIntError(){
		assertEquals("Not the correct value", 8, Level.ERROR.toInt());
	}
	
	public void testToIntWarn(){
		assertEquals("Not the correct value", 4, Level.WARN.toInt());
	}
	
	public void testToIntInfo(){
		assertEquals("Not the correct value", 2, Level.INFO.toInt());
	}
	
	public void testToIntDebug(){
		assertEquals("Not the correct value", 1, Level.DEBUG.toInt());
	}
	
	public void testToIntTrace(){
		assertEquals("Not the correct value", 0, Level.TRACE.toInt());
	}
}
