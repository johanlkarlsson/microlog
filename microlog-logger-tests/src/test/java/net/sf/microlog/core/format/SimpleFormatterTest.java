/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.core.format;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.format.SimpleFormatter;
import junit.framework.TestCase;

/**
 * Class to the <code>SimpleFormatterTest</code>.
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class SimpleFormatterTest extends TestCase {

	private SimpleFormatter testObject;

	protected void setUp() throws Exception {
		testObject = new SimpleFormatter();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testFormat() {
		testObject = new SimpleFormatter();
		String formattedString = testObject.format("clientID", "",
				0, Level.DEBUG, "message", null);
		assertNotNull("The formatted string must not be null.", formattedString);
		assertEquals("The formatted string is not correct.", "clientID 0:[DEBUG]-message", formattedString);
	}
	
	
	public void testFormatException() {
		testObject = new SimpleFormatter();
		String formattedString = testObject.format(null, "",
				0, Level.DEBUG, "message", new Exception("exception"));
		assertNotNull("The formatted string must not be null.", formattedString);
		assertEquals("The formatted string is not correct.", "0:[DEBUG]-message-java.lang.Exception: exception", formattedString);
	}
	
	public void testGetDefaultLimiter(){
		testObject = new SimpleFormatter();
		
		assertEquals("Not the correct default delimiter.", SimpleFormatter.DEFAULT_DELIMITER, testObject.getDelimiter());
	}
	
	
	public void testFormatTwice(){
		testObject = new SimpleFormatter();
		String formattedString = testObject.format(null, "",
				0, Level.DEBUG, "message", null);
		
		formattedString = testObject.format(null, "",
				0, Level.DEBUG, "message", null);
		assertNotNull("The formatted string must not be null.", formattedString);
		assertEquals("The formatted string is not correct.", "0:[DEBUG]-message", formattedString);
	}
	
	public void testFormatNullMessage(){
		testObject = new SimpleFormatter();
		String formattedString = testObject.format(null, "",
				0, Level.DEBUG, null, new Exception());
		assertNotNull("The formatted string must not be null.", formattedString);
		assertEquals("The formatted string is not correct.", "0:[DEBUG]-java.lang.Exception", formattedString);
	}
}
