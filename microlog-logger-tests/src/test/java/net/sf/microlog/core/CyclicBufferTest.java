/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.microlog.core;

import junit.framework.TestCase;



/**
 * A test class for the <code>CyclicBuffer</code> class.
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public class CyclicBufferTest extends TestCase {

	public void testLength() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer(5);
		Object object1 = new Object();
		cyclicBuffer.add(object1);
		Object object2 = new Object();
		cyclicBuffer.add(object2);

		assertEquals("Not the correct length.", 2, cyclicBuffer.length());
	}

	public void testLengthNoObjectsAdded() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer();
		assertEquals("The length shall be 0, since no object are added.", 0,
				cyclicBuffer.length());
	}

	public void testResize() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer(12);
		cyclicBuffer.add("microlog");
		assertEquals("Not the correct size.", 12, cyclicBuffer.getBufferSize());

		cyclicBuffer.resize(2);

		assertEquals("Not the correct size..", 2, cyclicBuffer.getBufferSize());
		assertNull("There shall be no objects in the buffer.", cyclicBuffer
				.get());
	}

	public void testResizeNegativeSize() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer(12);

		try {
			cyclicBuffer.resize(-1);
			fail("An IllegalArgumentException was expected.");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}

	}

	public void testAddObject() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer();

		cyclicBuffer.add("microlog");

		assertEquals("Not the correct size of the buffer.", 1, cyclicBuffer
				.length());
		assertEquals("The currentIndex is not correct.", 0,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", 0,
				cyclicBuffer.currentOldestIndex);
	}

	public void testAddObjects() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer(5);

		cyclicBuffer.add("microlog");

		assertEquals("Not the correct size of the buffer.", 1, cyclicBuffer
				.length());
		assertEquals("The currentIndex is not correct.", 0,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", 0,
				cyclicBuffer.currentOldestIndex);

		cyclicBuffer.add("another logmessage");

		assertEquals("Not the correct size of the buffer.", 2, cyclicBuffer
				.length());
		assertEquals("The currentIndex is not correct.", 1,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", 0,
				cyclicBuffer.currentOldestIndex);
	}

	public void testAddObjectsMoreThanSize() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer(2);

		cyclicBuffer.add("microlog");

		assertEquals("Not the correct size of the buffer.", 1, cyclicBuffer
				.length());
		assertEquals("The currentIndex is not correct.", 0,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", 0,
				cyclicBuffer.currentOldestIndex);

		cyclicBuffer.add("another logmessage");

		assertEquals("Not the correct size of the buffer.", 2, cyclicBuffer
				.length());
		assertEquals("The currentIndex is not correct.", 1,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", 0,
				cyclicBuffer.currentOldestIndex);

		cyclicBuffer.add("yet another logmessage");

		assertEquals("Not the correct size of the buffer.", 2, cyclicBuffer
				.length());
		assertEquals("The currentIndex is not correct.", 0,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", 1,
				cyclicBuffer.currentOldestIndex);
	}

	public void testAddObjectNull() {
		try {
			CyclicBuffer cyclicBuffer = new CyclicBuffer();
			cyclicBuffer.add(null);
			fail("An IllegalArgumentException was expected.");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}
	}

	public void testGet() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer(5);
		Object object1 = "1";
		cyclicBuffer.add(object1);
		Object object2 = "2";
		cyclicBuffer.add(object2);

		Object fetchedObject1 = cyclicBuffer.get();
		assertEquals("This is not the correct Object", object1, fetchedObject1);
		assertEquals("The currentOldestIndex is not correct", 1,
				cyclicBuffer.currentOldestIndex);

		Object fetchedObject2 = cyclicBuffer.get();
		assertEquals("This is not the correct Object", object2, fetchedObject2);
		assertEquals("The currentOldestIndex is not correct", -1,
				cyclicBuffer.currentOldestIndex);

		Object nullObject = cyclicBuffer.get();
		assertNull(
				"The fetched object shall be null, since no more objects are in the buffer.",
				nullObject);
		assertEquals("The currentOldestIndex is not correct", -1,
				cyclicBuffer.currentOldestIndex);
	}

	public void testGetMoreThanSize() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer(3);
		Object object1 = "1";
		cyclicBuffer.add(object1);
		Object object2 = "2";
		cyclicBuffer.add(object2);
		Object object3 = "3";
		cyclicBuffer.add(object3);
		Object object4 = "4";
		cyclicBuffer.add(object4);

		Object fetchedObject1 = cyclicBuffer.get();
		assertEquals("This is not the correct Object", object2, fetchedObject1);
		assertEquals("Not the correct length.", 2, cyclicBuffer.length());
		assertEquals("The currentIndex is not correct.", 0,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", 2,
				cyclicBuffer.currentOldestIndex);

		Object fetchedObject2 = cyclicBuffer.get();
		assertEquals("This is not the correct Object", object3, fetchedObject2);
		assertEquals("Not the correct length.", 1, cyclicBuffer.length());
		assertEquals("The currentIndex is not correct.", 0,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", 0,
				cyclicBuffer.currentOldestIndex);

		Object fetchedObject3 = cyclicBuffer.get();
		assertEquals("This is not the correct Object", object4, fetchedObject3);
		assertEquals("Not the correct length.", 0, cyclicBuffer.length());
		assertEquals("The currentIndex is not correct.", -1,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", -1,
				cyclicBuffer.currentOldestIndex);

		Object nullObject = cyclicBuffer.get();
		assertNull(
				"The fetched object shall be null, since no more objects are in the buffer.",
				nullObject);
		assertEquals("The currentIndex is not correct.", -1,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct", -1,
				cyclicBuffer.currentOldestIndex);
	}

	public void testGetNoObjectInBuffer() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer();
		assertNull(
				"Null must be returned since there are no objects in the buffer.",
				cyclicBuffer.get());
	}

	public void testClearBuffer() {
		CyclicBuffer cyclicBuffer = new CyclicBuffer(10);
		Object object1 = new Object();
		cyclicBuffer.add(object1);
		Object object2 = new Object();
		cyclicBuffer.add(object2);
		assertEquals("Not the correct length.", 2, cyclicBuffer.length());

		cyclicBuffer.clear();

		assertEquals("Not the correct length.", 0, cyclicBuffer.length());
		assertNull("It was expected that no object was returned.", cyclicBuffer
				.get());
		assertEquals("The currentIndex is not correct.", -1,
				cyclicBuffer.currentIndex);
		assertEquals("The currentOldestIndex is not correct.", -1,
				cyclicBuffer.currentOldestIndex);
	}
}
