/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.midp.example;

import java.io.IOException;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.Logger;
import net.sf.microlog.core.LoggerFactory;
import net.sf.microlog.core.format.PatternFormatter;
import net.sf.microlog.midp.file.FileAppender;

/**
 * An example midlet that shows how to use the <code>FileAppender</code>,
 * which uses a <code>FileConnection</code>. This requires MIDP 2.0 and
 * implementation of JSR-75 FileConnection.
 * 
 * @author Johan Karlsson
 * @since 1.1
 */
public class FileLogMidlet extends MIDlet implements CommandListener {

	private final static Logger log = LoggerFactory.getLogger();

	private Display display;
	private Form form;
	private Command logCommand;
	private Command closeCommand;
	private Command exitCommand;
	private int nofLoggedMessages;

	private FileAppender appender;

	public FileLogMidlet() {
		super();
		appender = new FileAppender();
		// Specify the filename and the FileAppender finds a suitable directory
		// to put the file.
		appender.setFileName("ExampleFileLog.txt");
		// You could specify the full path to the file if you prefer it, see
		// below.
		// appender.setFileName("c:/other/ExampleFileLog.txt");
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%r %m %T");
		appender.setFormatter(formatter);
		log.addAppender(appender);
		log.setLevel(Level.DEBUG);
		log.info("Setup of log finished");
		log.debug("Message 1");
		log.debug("Message 2");
		log.debug("Message 3");
		log.debug("Message 4");
		log.error("Sending an error message.");
	}

	protected void startApp() throws MIDletStateChangeException {
		log.info("Starting app");

		if (display == null) {
			display = Display.getDisplay(this);
			form = new Form("FileLogMIDlet");
			logCommand = new Command("DoLog", Command.SCREEN, 1);
			form.addCommand(logCommand);
			closeCommand = new Command("Close", "Close the log", Command.SCREEN, 2);
			form.addCommand(closeCommand);
			exitCommand = new Command("Exit", Command.EXIT, 1);
			form.addCommand(exitCommand);
			form.setCommandListener(this);
		}

		display.setCurrent(form);
	}

	protected void pauseApp() {
		log.info("Pausing app");

	}

	protected void destroyApp(boolean conditional)
			throws MIDletStateChangeException {
		log.info("Destroying app");
		LoggerFactory.shutdown();
		notifyDestroyed();
	}

	public void commandAction(Command command, Displayable displayable) {
		if (command == logCommand) {
			logMessage();
		} else if(command == closeCommand){
			try {
				appender.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if (command == exitCommand) {
			notifyDestroyed();
		}

	}

	/**
	 * Log a message.
	 */
	private void logMessage() {
		if(!appender.isLogOpen()){
			try {
				appender.open();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		log.debug("User pressed the log button. " + nofLoggedMessages++);
		
		log.debug("logSize() "+appender.getLogSize());
		log.debug("usedSize() "+appender.usedSize());
		log.debug("totalSize() "+appender.totalSize());
	}

}
