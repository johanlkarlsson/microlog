/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.midp.example;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.Logger;
import net.sf.microlog.core.LoggerFactory;
import net.sf.microlog.core.format.PatternFormatter;
import net.sf.microlog.midp.appender.s3.S3BufferAppender;

/**
 * An example midlet that shows how to use the <code>S3BufferAppender</code>.
 * This an S3 library for Java ME (J2ME). This is bundled with Microlog for your
 * convenience.
 * 
 * @author Johan Karlsson
 * @since 0.6
 */
public class S3BufferLogMidlet extends MIDlet implements CommandListener {

	private final static Logger log = LoggerFactory.getLogger();

	private Display display;
	private Form form;
	private Command logCommand;
	private Command logErrorCommand;
	private Command exitCommand;
	private int nofLoggedMessages;

	public S3BufferLogMidlet() {
		S3BufferAppender appender = new S3BufferAppender();
		// You must set the accessKeyID & secretAccessKey in order for this to
		// work.
		// For more information, see your Amazon account.
		appender.setAccessKeyID("accessKeyID");
		appender.setSecretAccessKey("secretAccessKey");
		PatternFormatter formatter = new PatternFormatter();
		formatter.setPattern("%r %m %T");
		appender.setFormatter(formatter);
		log.addAppender(appender);
		log.setLevel(Level.DEBUG);
		log.info("Setup of log finished");
		
		log.error("This error message shall trigger file => S3.");
	}

	protected void startApp() throws MIDletStateChangeException {
		log.info("Starting app");

		if (display == null) {
			display = Display.getDisplay(this);
			form = new Form("S3LogMIDlet");
			logCommand = new Command("DoLog", Command.SCREEN, 1);
			form.addCommand(logCommand);
			logErrorCommand = new Command("LogError",
					"Log a message with error level.", Command.SCREEN, 2);
			form.addCommand(logErrorCommand);
			exitCommand = new Command("Exit", Command.EXIT, 1);
			form.addCommand(exitCommand);
			form.setCommandListener(this);
		}

		display.setCurrent(form);
	}

	protected void pauseApp() {
		log.info("Pausing app");

	}

	protected void destroyApp(boolean conditional)
			throws MIDletStateChangeException {
		log.info("Destroying app");
		LoggerFactory.shutdown();
		notifyDestroyed();
	}

	public void commandAction(Command command, Displayable displayable) {
		if (command == logCommand) {
			logMessage();
		} else if (command == logErrorCommand) {
			logErrorMessage();
		} else if (command == exitCommand) {
			notifyDestroyed();
		}

	}

	/**
	 * Log a message with trace level.
	 */
	private void logMessage() {
		log.debug("User pressed the log button. " + nofLoggedMessages++);
	}

	/**
	 * Log a message with error level.
	 */
	private void logErrorMessage() {
		log.error("This error message shall trigger file => S3.");
	}

}
