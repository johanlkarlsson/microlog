/*
 * Copyright 2009 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.midp.example;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import net.sf.microlog.core.Logger;
import net.sf.microlog.core.LoggerFactory;
import net.sf.microlog.core.config.PropertyConfigurator;
import net.sf.microlog.midp.example.a.Foo;
import net.sf.microlog.midp.example.b.Bar;
import net.sf.microlog.midp.example.b.c.Bick;

/**
 * This MIDlet shows how to use a property file for configuration, the
 * "Log4j"-style. Please notice that the classes in sub-packages a & b must be
 * included in order for this example to work.
 * 
 * @author Johan Karlsson
 * 
 * @since 2.0
 * 
 */
public class PropertyConfiguratorMidletV2 extends MIDlet implements
		CommandListener {

	private static final Logger log = LoggerFactory.getLogger(PropertyConfiguratorMidletV2.class);

	private static boolean firstTime = true;

	private Display display;
	private Form form = new Form("PropertiesConfigMIDletV2micrologV2.properties");
	private Command logCommand;
	private Command exitCommand;

	public PropertyConfiguratorMidletV2() {

	}

	protected void startApp() throws MIDletStateChangeException {

		// We only want to configure Microlog the first time
		if (firstTime) {
			// Specify which property file to use.
			PropertyConfigurator.configure("/micrologV2.properties");
			log.info("startApp() first time");
			firstTime = false;
		} else {
			log.info("startApp() again");
		}

		if (display == null) {
			log.debug("Creating Display object & initializing our Form.");
			display = Display.getDisplay(this);
			logCommand = new Command("DoLog", Command.SCREEN, 1);
			form.addCommand(logCommand);
			exitCommand = new Command("Exit", Command.EXIT, 1);
			form.addCommand(exitCommand);
			form.setCommandListener(this);
		}

		log.debug("Display our form.");
		display.setCurrent(form);

		// Do some stuff
		Foo foo = new Foo();
		foo.foo();

		Bar bar = new Bar();
		bar.bar();

		Bick bick = new Bick();
		bick.bar();
	}

	protected void pauseApp() {
		log.info("pauseApp()");
	}

	protected void destroyApp(boolean conditional)
			throws MIDletStateChangeException {
		log.info("destroyApp()");
		LoggerFactory.shutdown();
		notifyDestroyed();
	}

	public void commandAction(Command command, Displayable displayable) {
		log.debug("commandAction()");

		if (command == exitCommand) {
			log
					.debug("Calling notifyDestroyed() to inform the AMS that we want to quit.");
			notifyDestroyed();
		}
	}
}
