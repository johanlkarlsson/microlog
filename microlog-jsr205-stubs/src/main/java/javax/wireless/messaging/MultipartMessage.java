package javax.wireless.messaging;

/**
 * This is a dummy interface.
 * 
 * @author Johan Karlsson
 * 
 */
public interface MultipartMessage extends Message {

	public void setHeader(String headerField, String headerValue);

	public void setSubject(String subject);

	public void addAddress(String type, String address);

	public void addMessagePart(MessagePart part);
}
