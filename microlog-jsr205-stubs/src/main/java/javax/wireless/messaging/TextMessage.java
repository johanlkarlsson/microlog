package javax.wireless.messaging;

/**
 * This is a dummy interface.
 * 
 * @author Johan Karlsson
 *
 */
public interface TextMessage extends Message {
	
	public String getPayloadText();
	
	public void setPayloadText(String text);
}
