package javax.wireless.messaging;

/**
 * This is a mock class.
 * 
 * @author Johan Karlsson
 *
 */
public class MessagePart {
	
	private byte[] data;

	public MessagePart(byte[] data, String a, String b, String c, String d) {
		this.data = data;
	}

	public byte[] getData() {
		return data;
	}
	
	
}
