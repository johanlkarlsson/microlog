package javax.wireless.messaging;

import java.util.Date;

/**
 * This is a dummy interface.
 * 
 * @author Johan Karlsson
 *
 */
public interface Message {
	
	public void setAddress(String address);
	
	public String getAddress();
	
	public Date getTimestamp();
	
}
