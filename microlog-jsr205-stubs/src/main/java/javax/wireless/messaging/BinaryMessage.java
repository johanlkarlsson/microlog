package javax.wireless.messaging;

public interface BinaryMessage extends Message {
	
	
	public void setPayloadData(byte[] data);
	
	public byte[] getPayloadData();
	
	
}
