package javax.wireless.messaging;

import java.io.IOException;

import javax.microedition.io.Connection;

/**
 * This is a dummy interface.
 * 
 * @author Johan Karlsson
 * 
 */
public interface MessageConnection extends Connection {

	public static final String MULTIPART_MESSAGE = "multipart";

	public static final String TEXT_MESSAGE = "text";
	
	public static final String BINARY_MESSAGE = "binarys";

	public Message newMessage(String type);
	
	public Message newMessage(String type, String address);
	
	public void setMessageListener(MessageListener listener);

	public void send(Message message) throws IOException;
	
}
