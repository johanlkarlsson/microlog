package net.sf.microlog.jsr205.mock;

import javax.wireless.messaging.TextMessage;

public class DefaultTextMessage extends AbstractMessage implements TextMessage{
	
	private String payloadText;

	public String getPayloadText() {
		return payloadText;
	}

	public void setPayloadText(String text) {
		this.payloadText = text;
	}

}
