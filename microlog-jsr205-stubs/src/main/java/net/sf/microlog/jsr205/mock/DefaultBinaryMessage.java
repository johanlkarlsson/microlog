package net.sf.microlog.jsr205.mock;

import javax.wireless.messaging.BinaryMessage;

public class DefaultBinaryMessage extends AbstractMessage implements BinaryMessage {
	
	private byte[] data;

	public byte[] getPayloadData() {
		return data;
	}

	public void setPayloadData(byte[] data) {
		this.data = data;
	}

}
