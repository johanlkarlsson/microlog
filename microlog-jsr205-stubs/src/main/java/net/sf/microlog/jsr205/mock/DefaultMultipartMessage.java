package net.sf.microlog.jsr205.mock;

import java.util.Hashtable;
import java.util.Vector;

import javax.wireless.messaging.MessagePart;
import javax.wireless.messaging.MultipartMessage;

public class DefaultMultipartMessage extends AbstractMessage implements
		MultipartMessage {

	private static final String ADDRESS_TYPE_TO = "to";

	private static final String ADDRESS_TYPE_CC = "cc";

	private static final String ADDRESS_TYPE_BCC = "bcc";

	private Vector toAddresses = new Vector(8);

	private Vector ccAddresses = new Vector(8);

	private Vector bccAddresses = new Vector(8);

	private Vector messageParts = new Vector(16);

	private Hashtable headerFieldMap = new Hashtable(21);

	private String subject;

	public void addAddress(String type, String address) {
		if (type == null || address == null) {
			throw new IllegalArgumentException("The type and adress must not be null.");
		}

		if (type.equals(ADDRESS_TYPE_TO)) {
			toAddresses.addElement(address);
		} else if (type.equals(ADDRESS_TYPE_CC)) {
			ccAddresses.addElement(address);
		} else if (type.equals(ADDRESS_TYPE_BCC)) {
			bccAddresses.addElement(address);
		} else {
			throw new IllegalArgumentException("Not a correct address type.");
		}
	}

	public void addMessagePart(MessagePart part) {
		messageParts.addElement(part);
	}

	public void setHeader(String headerField, String headerValue) {
		headerFieldMap.put(headerField, headerValue);
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}
	
	

}
