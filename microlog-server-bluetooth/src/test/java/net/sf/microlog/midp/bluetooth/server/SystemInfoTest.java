package net.sf.microlog.midp.bluetooth.server;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class SystemInfoTest {
	
	@Test
	public void testGetValueOsName() {
		assertTrue(SystemInfo.OS_NAME.getValue().length() > 0);
	}
	
	@Test
	public void testGetValueOsArchitecture() {
		assertTrue(SystemInfo.OS_ARCHITECTURE.getValue().length() > 0);
	}
	
	@Test
	public void testGetValueOsVersion() {
		assertTrue(SystemInfo.OS_VERSION.getValue().length() > 0);
	}
	
	@Test
	public void testGetValueJavaVersion() {
		assertTrue(SystemInfo.JAVA_VERSION.getValue().length() > 0);
	}
}
