package net.sf.microlog.midp.bluetooth.server;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class BluetoothConnectionHandlerTest {
	private BluetoothConnectionHandler connectionHandler;

	@Before
	public void setUp() {
		connectionHandler = BluetoothConnectionHandler.UTIL;
	}

	@Test
	public void testAddConnection() {
		connectionHandler.addConnection(new BluetoothStreamReaderThreadImpl(
				new DefaultServerListenerImpl(), null, null));

		assertTrue(connectionHandler.getConnectionMapTestOnly().size() == 1);
	}

	@Test
	public void testRemoveConnection() {
		connectionHandler.addConnection(new BluetoothStreamReaderThreadImpl(
				new DefaultServerListenerImpl(), null, null));
		connectionHandler.removeConnection(1);

		assertTrue(connectionHandler.getConnectionMapTestOnly().size() == 0);
	}

	@Test
	public void testGracefulShutdown() {
		connectionHandler.addConnection(new BluetoothStreamReaderThreadImpl(
				new DefaultServerListenerImpl(), null, null));
		connectionHandler.addConnection(new BluetoothStreamReaderThreadImpl(
				new DefaultServerListenerImpl(), null, null));

		connectionHandler.gracefulShutdown(null);
		
		assertTrue(connectionHandler.getConnectionMapTestOnly().size() == 0);
	}
}
