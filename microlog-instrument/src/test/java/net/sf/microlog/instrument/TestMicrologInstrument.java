/*
 * Copyright 2008 The MicroLog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.microlog.instrument;

import java.lang.reflect.Method;
import java.net.URL;

import junit.framework.TestCase;
import net.sf.foo.Foo;
import net.sf.jour.InstrumentingClassLoader;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the instrumentor.
 * 
 * @author Karsten Ohme
 * 
 */
public class TestMicrologInstrument extends TestCase {

	/**
	 * The class instance to test.
	 */
	private Object instance;

	/**
	 * The class to test.
	 */
	private Class clazz;

	/**
	 * Sets up the test and instruments a test class.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Before
    @Override
	public void setUp() throws Exception {
		String testClassName = Foo.class.getName();

		String resource = "/" + testClassName.replace('.', '/') + ".class";

		URL url = this.getClass().getResource(resource);

		String path = url.toExternalForm();
		path = path.substring("file:".length(), path.length()
				- resource.length());

		String[] paths = new String[] { path };

		InstrumentingClassLoader cl = new InstrumentingClassLoader(
				"/microlog.jour.xml", paths);

		clazz = cl.loadClass(testClassName);
		instance = clazz.newInstance();
	}

	/**
	 * Test simple logging.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testLog() throws Exception {
		Method method = clazz.getMethod("log", null);
		method.invoke(instance, null);
	}

	/**
	 * Test logging of exception.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testLogException() throws Exception {
		Method method = clazz.getMethod("logException", null);
		method.invoke(instance, null);
	}

	/**
	 * Test explicit logging.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testExplicitLog() throws Exception {
		Method method = clazz.getMethod("crazyLog", null);
		method.invoke(instance, null);
	}

	/**
	 * Test explicit logging of exception.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testExplicitLogException() throws Exception {
		Method method = clazz.getMethod("crazyLogException", null);
		method.invoke(instance, null);
	}

	/**
	 * Tests a synchronized method.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testMySynchronized() throws Exception {
		Method method = clazz.getMethod("mySynchronized", null);
		method.invoke(instance, null);
	}

	/**
	 * Test a method returning an integer.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testReturnInt() throws Exception {
		Method method = clazz.getMethod("returnInt", null);
		method.invoke(instance, null);
	}

	/**
	 * Test a method declaring an Exception.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testDeclaringException() throws Exception {
		Method method = clazz.getMethod("declaringException", null);
		method.invoke(instance, null);
	}

	/**
	 * Test a method with a param.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testWithParam() throws Exception {
		Method method = clazz.getMethod("withParam", int.class);
		method.invoke(instance, 5);
	}

	/**
	 * Test method declaring an Exception and returning an integer.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testDeclaringExceptionAndReturningInt() throws Exception {
		Method method = clazz.getMethod("declaringExceptionAndReturningInt",
				null);
		method.invoke(instance, null);
	}

}
