/*
 * Copyright 2008 The MicroLog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.microlog.instrument;

import java.lang.reflect.Method;
import java.net.URL;

import junit.framework.TestCase;
import net.sf.foo.Bar;
import net.sf.jour.InstrumentingClassLoader;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests a not instrumented class.
 * 
 * @author Karsten Ohme
 * 
 */
public class TestNotInstrumentedClass extends TestCase {

	/**
	 * The class instance to test.
	 */
	private Object notInstrumentedInstance;

	/**
	 * A non instrumented class to test.
	 */
	private Class notInstrumentedClazz;

	/**
	 * Sets up the test and instruments a test class.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Before
    @Override
	public void setUp() throws Exception {
		// not instrumented
		String notInstrumentedTestClassName = Bar.class.getName();
		String notInstrumentedResource = "/"
				+ notInstrumentedTestClassName.replace('.', '/') + ".class";

		URL notInstrumentedURL = this.getClass().getResource(
				notInstrumentedResource);

		String notInstrumentedPath = notInstrumentedURL.toExternalForm();
		notInstrumentedPath = notInstrumentedPath
				.substring("file:".length(), notInstrumentedPath.length()
						- notInstrumentedResource.length());

		String[] paths = new String[] { notInstrumentedPath };

		InstrumentingClassLoader cl = new InstrumentingClassLoader(
				"/microlog.jour.xml", paths);

		notInstrumentedClazz = cl.loadClass(notInstrumentedTestClassName);
		notInstrumentedInstance = notInstrumentedClazz.newInstance();
	}

	/**
	 * Test a not instrumented class.
	 * 
	 * @throws Exception
	 *             if an exception occurs.
	 */
    @Test
	public void testNotInstrumentedClass() throws Exception {
		Method method = notInstrumentedClazz.getMethod("notInstrumentedMethod",
				null);
		method.invoke(notInstrumentedInstance, null);
	}

}
