/*
 * Copyright 2008 The MicroLog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.foo;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.Logger;
import net.sf.microlog.core.LoggerFactory;

/**
 * Class to be instrumented.
 * 
 * @author Karsten Ohme
 * 
 */
public class Foo {

	/**
	 * Logger instance.
	 */
	private final static Logger LOG = LoggerFactory.getLogger(Foo.class.getName());

	/**
	 * Logs message.
	 */
	public void log() {
		LOG.debug("Test message");
	}

	/**
	 * Logs an exception.
	 */
	public void logException() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test message1");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Parsing number");
			}
			Integer.parseInt("abc");
		} catch (NumberFormatException e) {
			LOG.error("Could not parse number", e);
		}
		LOG.debug("Test message2", new RuntimeException("Test Message2"));
		LOG.debug("Test message3", new RuntimeException("Test Message3"));
	}

	/**
	 * A synchronized method.
	 */
	public synchronized void mySynchronized() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test synchronized");
		}
	}

	/**
	 * A method returning an integer.
	 * 
	 * @return an integer
	 */
	public int returnInt() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test return int");
		}
		return 1;
	}

	/**
	 * A method declaring an Exception.
	 * 
	 */
	public void declaringException() throws Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test declaring exception");
		}
	}

	/**
	 * A method declaring an Exception and returning an integer.
	 * 
	 * @return an integer.
	 */
	public int declaringExceptionAndReturningInt() throws Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test declaring exception and returning int");
		}
		return 1;
	}

	/**
	 * Explicit logging.
	 */
	public void crazyLog() {
		LOG.log(Level.DEBUG, "Test message");
	}

	/**
	 * Explicit logging of an exception.
	 */
	public void crazyLogException() {
		LOG.log(Level.DEBUG, "Test message", new RuntimeException(
				"Test Message"));
	}

	/**
	 * A method with a parameter.
	 * 
	 * @param The
	 *            parameter.
	 */
	public void withParam(int param) {
		LOG.log(Level.DEBUG, "With param");
	}
}
