/*
 * Copyright 2008 The MicroLog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.foo;

import net.sf.microlog.core.Level;
import net.sf.microlog.core.Logger;
import net.sf.microlog.core.LoggerFactory;


/**
 * Class to be instrumented.
 * 
 * @author Karsten Ohme
 * 
 */
public class Foo {

	/**
	 * The logger instance.
	 */
	private final static Logger LOG = LoggerFactory.getLogger(Foo.class.getName());

	/**
	 * Logs a message.
	 */
	public void log() {
		LOG.debug("Test message");
		LOG.trace("Test message");
		LOG.info("Test message");
		LOG.warn("Test message");
		LOG.error("Test message");
		LOG.fatal("Test message");
	}

	/**
	 * Logs debug exceptions.
	 */
	public void logDebugExceptions() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test message1");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Parsing number");
			}
			Integer.parseInt("abc");
		} catch (NumberFormatException e) {
			LOG.error("Could not parse number", e);
		}
		LOG.debug("Test message2", new RuntimeException("Test Message2"));
		LOG.debug("Test message3", new RuntimeException("Test Message3"));
	}

	/**
	 * Logs a exception.
	 */
	public void logException() {
		LOG.debug("Test message", new RuntimeException("Test Message"));
		LOG.trace("Test message", new RuntimeException("Test Message"));
		LOG.info("Test message", new RuntimeException("Test Message"));
		LOG.warn("Test message", new RuntimeException("Test Message"));
		LOG.error("Test message", new RuntimeException("Test Message"));
		LOG.fatal("Test message", new RuntimeException("Test Message"));
	}

	/**
	 * Logs a exception.
	 */
	public void explicitLog() {
		LOG.log(Level.DEBUG, "Test message");
		LOG.log(Level.INFO, "Test message");
		LOG.log(Level.WARN, "Test message");
		LOG.log(Level.TRACE, "Test message");
		LOG.log(Level.ERROR, "Test message");
		LOG.log(Level.FATAL, "Test message");
	}

	/**
	 * Logs a exception.
	 */
	public void explicitLogException() {
		LOG.log(Level.DEBUG, "Test message", new RuntimeException(
				"Test Message"));
		LOG.log(Level.INFO, "Test message",
				new RuntimeException("Test Message"));
		LOG.log(Level.WARN, "Test message",
				new RuntimeException("Test Message"));
		LOG.log(Level.TRACE, "Test message", new RuntimeException(
				"Test Message"));
		LOG.log(Level.ERROR, "Test message", new RuntimeException(
				"Test Message"));
		LOG.log(Level.FATAL, "Test message", new RuntimeException(
				"Test Message"));
	}

	/**
	 * A synchronized method.
	 */
	public synchronized void mySynchronized() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test synchronized");
		}
	}

	/**
	 * A method returning an integer.
	 * 
	 * @return an integer
	 */
	public int returnInt() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test return int");
		}
		return 1;
	}

	/**
	 * A method declaring an Exception.
	 * 
	 */
	public void declaringException() throws Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test declaring exception");
		}
	}

	/**
	 * A method declaring an Exception and returning an integer.
	 * 
	 * @return an integer.
	 */
	public int declaringExceptionAndReturningInt() throws Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Test declaring exception and returning int");
		}
		return 1;
	}

	/**
	 * A method with a parameter.
	 * 
	 * @param param
	 *            The parameter.
	 */
	public void withParam(int param) {
		LOG.log(Level.DEBUG, "With param");
	}
}
