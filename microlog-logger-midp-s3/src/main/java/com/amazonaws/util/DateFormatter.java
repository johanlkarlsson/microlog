
/*
 * This software code is made available "AS IS" without warranties of any 
 * kind.  You may copy, display, modify and redistribute the software
 * code either by itself or as incorporated into your code; provided that
 * you do not remove any proprietary notices.  Your use of this software
 * code is at your own risk and you waive any claim against Amazon
 * Web Services LLC or its affiliates with respect to your use of
 * this software code. (c) Amazon Web Services LLC or its
 * affiliates.
 */


package com.amazonaws.util;

import java.util.Calendar;

public class DateFormatter {

    public static String formatTime( Calendar time ) {   	
        String yyyy = String.valueOf( time.get( Calendar.YEAR ) );
        String MM = String.valueOf( time.get( Calendar.MONTH ) + 1 );
        String dd = String.valueOf( time.get( Calendar.DATE ) );
        String HH = String.valueOf( time.get( Calendar.HOUR_OF_DAY ) );
        String mm = String.valueOf( time.get( Calendar.MINUTE ) );
        String ss = String.valueOf( time.get( Calendar.SECOND ) );
        String SSS = String.valueOf( time.get( Calendar.MILLISECOND ) );
        
        StringBuffer buffer = new StringBuffer( 30 );
        buffer.append( yyyy );
        buffer.append( "-" );
        
        if ( MM.length() == 1 ) {
            buffer.append( "0" );
        }
        buffer.append( MM );
        buffer.append( "-" );
        
        if ( dd.length() == 1 ) {
			buffer.append( "0" );
        }
        buffer.append( dd );
        buffer.append( "T" );
        
        if ( HH.length() == 1 ) {
            buffer.append( "0" );        
        }
        buffer.append( HH );
        buffer.append( ":" );
        
        
        if ( mm.length() == 1 ) {
            buffer.append( "0" );        
        }
        buffer.append( mm );
        buffer.append( ":" );

        if ( ss.length() == 1 ) {
            buffer.append( "0" );        
        }
        buffer.append( ss );
        buffer.append( "." );
        
        
        if ( SSS.length() == 1 ) {
            buffer.append( "00" );        
        } 
        else if ( SSS.length() == 2 ) {
            buffer.append( "0" );        
        }
        buffer.append( SSS );
        buffer.append( "Z" );
            
        return buffer.toString();    
    }      
    
    protected DateFormatter() {}
}
