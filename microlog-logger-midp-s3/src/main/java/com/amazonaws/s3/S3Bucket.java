
/*
 * This software code is made available "AS IS" without warranties of any 
 * kind.  You may copy, display, modify and redistribute the software
 * code either by itself or as incorporated into your code; provided that
 * you do not remove any proprietary notices.  Your use of this software
 * code is at your own risk and you waive any claim against Amazon
 * Web Services LLC or its affiliates with respect to your use of
 * this software code. (c) Amazon Web Services LLC or its
 * affiliates.
 */


package com.amazonaws.s3;

import java.io.ByteArrayInputStream;
import java.util.Vector;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * Representation of an Amazon S3 Bucket.
 *
 * @author Glenn Dierkes
 * @version 1.0 
 **/
public class S3Bucket {

    protected Vector names = new Vector();
	protected String bucketName = null;
	
	/**
	 * @return The name of the bucket.
	 **/
	public String getBucketName() {
		return this.bucketName;
	}

	/**
	 * @return The list of object keys in the bucket.
	 **/
    public Vector getNames() {
        return this.names;
    }
                
    protected static S3Bucket createBucket( String bucketName, String xml ) throws Exception {
        S3Bucket bucket = new S3Bucket();
        S3Bucket.extract( bucket, xml.getBytes() );
		bucket.bucketName = bucketName;
              
        return bucket;
    }

    protected static void extract( S3Bucket list, byte[] xml ) throws Exception {   
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance( "com.amazonaws.xml.KXmlParser", null );
        factory.setNamespaceAware( false );

        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput( new ByteArrayInputStream( xml ), "UTF-8" );
                
        String data = null;   
             
        int eventType = xpp.getEventType();
        while ( eventType != XmlPullParser.END_DOCUMENT ) {
            switch ( eventType ) {
                case XmlPullParser.START_TAG: {
                    if ( xpp.getName().equals( "Key" ) ) {
                        data = null;
                    }
                    
                    break;
                }
                case XmlPullParser.TEXT: {
                    data = xpp.getText();
                    break;
                }
                case XmlPullParser.END_TAG: {
                    if ( xpp.getName().equals( "Key" ) ) {
        				list.names.addElement( data );
                    }                    
                    
                    break;
                } 
            }
            
            eventType = xpp.next();
        }
    }   
    
}
