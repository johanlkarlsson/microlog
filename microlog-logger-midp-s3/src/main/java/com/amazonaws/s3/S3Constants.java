
/*
 * This software code is made available "AS IS" without warranties of any 
 * kind.  You may copy, display, modify and redistribute the software
 * code either by itself or as incorporated into your code; provided that
 * you do not remove any proprietary notices.  Your use of this software
 * code is at your own risk and you waive any claim against Amazon
 * Web Services LLC or its affiliates with respect to your use of
 * this software code. (c) Amazon Web Services LLC or its
 * affiliates.
 */


package com.amazonaws.s3;

import java.util.TimeZone;

public class S3Constants {

	public static final	TimeZone GMT = TimeZone.getTimeZone( "GMT" );
	
	protected static final String BUCKET_NAME    = "__BUCKET_NAME__";
	protected static final String OBJECT_KEY     = "__OBJECT_KEY__";
	protected static final String DATA		   = "__DATA__";
	protected static final String ACCESS_KEY_ID  = "__ACCESS_KEY_ID__";
	protected static final String TIMESTAMP      = "__TIMESTAMP__";
	protected static final String SIGNATURE      = "__SIGNATURE__";
	protected static final String CONTENT_LENGTH = "__CONTENT_LENGTH__";
	
    
    protected static final String SOAP_START = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soapenv:Body>"; 
    protected static final String SOAP_END = "</soapenv:Body></soapenv:Envelope>";

    
	protected static final String LIST_MY_BUCKETS = SOAP_START +
			"<ListAllMyBuckets xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">" +
				"<AWSAccessKeyId>__ACCESS_KEY_ID__</AWSAccessKeyId>" +
				"<Timestamp>__TIMESTAMP__</Timestamp>" + 
				"<Signature>__SIGNATURE__</Signature>" +
			"</ListAllMyBuckets>" + SOAP_END;
		
	protected static final String CREATE_BUCKET = SOAP_START +
			"<CreateBucket xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">" +
				"<Bucket>__BUCKET_NAME__</Bucket>" +
				"<AWSAccessKeyId>__ACCESS_KEY_ID__</AWSAccessKeyId>" +
				"<Timestamp>__TIMESTAMP__</Timestamp>" + 
				"<Signature>__SIGNATURE__</Signature>" +
			"</CreateBucket>" + SOAP_END;
		
	protected static final String DELETE_BUCKET = SOAP_START +
			"<DeleteBucket xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">" +
				"<Bucket>__BUCKET_NAME__</Bucket>" +
				"<AWSAccessKeyId>__ACCESS_KEY_ID__</AWSAccessKeyId>" +
				"<Timestamp>__TIMESTAMP__</Timestamp>" +
				"<Signature>__SIGNATURE__</Signature>" +
			"</DeleteBucket>" + SOAP_END;
		
	protected static final String LIST_BUCKET = SOAP_START +
			"<ListBucket xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">" +
				"<Bucket>__BUCKET_NAME__</Bucket>" +
				"<AWSAccessKeyId>__ACCESS_KEY_ID__</AWSAccessKeyId>" +
				"<Timestamp>__TIMESTAMP__</Timestamp>" +
				"<Signature>__SIGNATURE__</Signature>" +
			"</ListBucket>" + SOAP_END;
		
	protected static final String PUT_OBJECT = SOAP_START +
			"<PutObjectInline xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">" +
				"<Bucket>__BUCKET_NAME__</Bucket>" +
				"<Key>__OBJECT_KEY__</Key>" +
				"<Data>__DATA__</Data>" + 
				"<ContentLength>__CONTENT_LENGTH__</ContentLength>" +
				"<AWSAccessKeyId>__ACCESS_KEY_ID__</AWSAccessKeyId>" +
				"<Timestamp>__TIMESTAMP__</Timestamp>" +
				"<Signature>__SIGNATURE__</Signature>" +
			"</PutObjectInline>" + SOAP_END;
		
	protected static final String GET_OBJECT = SOAP_START +
			"<GetObject xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">" + 
				"<Bucket>__BUCKET_NAME__</Bucket>" + 
				"<Key>__OBJECT_KEY__</Key>" + 
				"<GetMetadata>false</GetMetadata>" + 
				"<GetData>true</GetData>" + 
				"<InlineData>true</InlineData>" + 
				"<AWSAccessKeyId>__ACCESS_KEY_ID__</AWSAccessKeyId>" + 
				"<Timestamp>__TIMESTAMP__</Timestamp>" + 
				"<Signature>__SIGNATURE__</Signature>" + 
			"</GetObject>" + SOAP_END;
		
	protected static final String DELETE_OBJECT = SOAP_START +
			"<DeleteObject xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">" +
				"<Bucket>__BUCKET_NAME__</Bucket>" +
				"<Key>__OBJECT_KEY__</Key>" +
				"<AWSAccessKeyId>__ACCESS_KEY_ID__</AWSAccessKeyId>" +
				"<Timestamp>__TIMESTAMP__</Timestamp>" +
				"<Signature>__SIGNATURE__</Signature>" +
			"</DeleteObject>" + SOAP_END;
		
}
