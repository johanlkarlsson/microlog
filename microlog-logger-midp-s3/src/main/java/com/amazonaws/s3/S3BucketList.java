
/*
 * This software code is made available "AS IS" without warranties of any 
 * kind.  You may copy, display, modify and redistribute the software
 * code either by itself or as incorporated into your code; provided that
 * you do not remove any proprietary notices.  Your use of this software
 * code is at your own risk and you waive any claim against Amazon
 * Web Services LLC or its affiliates with respect to your use of
 * this software code. (c) Amazon Web Services LLC or its
 * affiliates.
 */


package com.amazonaws.s3;

import java.io.ByteArrayInputStream;
import java.util.Vector;

import org.xmlpull.v1.*;

/**
 * List of Amazon S3 Buckets.
 *
 * @author Glenn Dierkes
 * @version 1.0 
 **/
public class S3BucketList {

    protected Vector buckets = new Vector();

	/**
	 * @return A list of Buckets.
	 **/
    public Vector getBuckets() {
        return this.buckets;
    }
    
    protected static S3BucketList createBucketList( String xml ) throws Exception {
        S3BucketList bucketList = new S3BucketList();
        S3BucketList.extract( bucketList, xml.getBytes() );
              
        return bucketList;
    }

    protected static void extract( S3BucketList list, byte[] xml ) throws Exception {   
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance( "com.amazonaws.xml.KXmlParser", null );
        factory.setNamespaceAware( false );

        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput( new ByteArrayInputStream( xml ), "UTF-8" );
                
        String data = null;   
             
        int eventType = xpp.getEventType();
        while ( eventType != XmlPullParser.END_DOCUMENT ) {
            switch ( eventType ) {
                case XmlPullParser.START_TAG: {
                    if ( xpp.getName().equals( "Name" ) ) {
                        data = null;
                    }
                    
                    break;
                }
                case XmlPullParser.TEXT: {
                    data = xpp.getText();
                    break;
                }
                case XmlPullParser.END_TAG: {
                    if ( xpp.getName().equals( "Name" ) ) {
                        list.buckets.addElement( data );
                    }                    
                    
                    break;
                } 
            }
            
            eventType = xpp.next();
        }
    }   
    
}
