
/*
 * This software code is made available "AS IS" without warranties of any 
 * kind.  You may copy, display, modify and redistribute the software
 * code either by itself or as incorporated into your code; provided that
 * you do not remove any proprietary notices.  Your use of this software
 * code is at your own risk and you waive any claim against Amazon
 * Web Services LLC or its affiliates with respect to your use of
 * this software code. (c) Amazon Web Services LLC or its
 * affiliates.
 */


package com.amazonaws.s3;

import java.io.ByteArrayInputStream;

import org.xmlpull.v1.*;

/**
 * Representation of an Amazon S3 Object.
 *
 * @author Glenn Dierkes
 * @version 1.0 
 **/
public class S3Object {

    protected String data = null;
	protected String key = null;

	/**
	 * @return The object's key.
	 **/
	public String getKey() {
		return this.key;
	}

	/**
	 * @return The object's data.
	 **/
    public String getData() {
        return this.data;
    }
                
    protected static S3Object createObject( String key, String xml ) throws Exception {
        S3Object object = new S3Object();
        S3Object.extract( object, xml.getBytes() );
		object.key = key;
              
        return object;
    }

    protected static void extract( S3Object object, byte[] xml ) throws Exception {   
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance( "com.amazonaws.xml.KXmlParser", null );
        factory.setNamespaceAware( false );

        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput( new ByteArrayInputStream( xml ), "UTF-8" );
                
        String data = null;   
             
        int eventType = xpp.getEventType();
        while ( eventType != XmlPullParser.END_DOCUMENT ) {
            switch ( eventType ) {
                case XmlPullParser.START_TAG: {
                    if ( xpp.getName().equals( "Data" ) ) {
                        data = null;
                    }
                    
                    break;
                }
                case XmlPullParser.TEXT: {
                    data = xpp.getText();
                    break;
                }
                case XmlPullParser.END_TAG: {
                    if ( xpp.getName().equals( "Data" ) ) {
                        object.data = data;
                    }                    
                    
                    break;
                } 
            }
            
            eventType = xpp.next();
        }
    }   
    
}
