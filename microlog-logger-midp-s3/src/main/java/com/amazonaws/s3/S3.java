
/*
 * This software code is made available "AS IS" without warranties of any 
 * kind.  You may copy, display, modify and redistribute the software
 * code either by itself or as incorporated into your code; provided that
 * you do not remove any proprietary notices.  Your use of this software
 * code is at your own risk and you waive any claim against Amazon
 * Web Services LLC or its affiliates with respect to your use of
 * this software code. (c) Amazon Web Services LLC or its
 * affiliates.
 */


package com.amazonaws.s3;

import java.util.Calendar;

import com.amazonaws.util.DateFormatter;
import com.amazonaws.crypto.HMACSigner;
import amazon.soap.SoapRequest;


/**
 * Represents the main class for interaction with <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/">Amazon S3</a> 
 * To use this class you must be able to provide a valid Amazon S3 account Access Key Id and Secret Key.
 * Member operations reflect the operations supported by Amazon S3.
 * <p>
 * This toolkit was designed to be used in both J2SE and J2ME environments.
 * <p>
 * All communications with Amazon S3 are done using SOAP.
 *
 * @author Glenn Dierkes
 * @version 1.0 
 **/
public class S3 {

	/**
	 * String representing the value <b>https://s3.amazonaws.com/soap</b>
     *
     * It is recommended that all SOAP communication that contains a singature be done over SSL. 
     *
	 **/
	public static final String HTTPS_URL = "https://s3.amazonaws.com/soap";


    private static byte[] buffer = new byte[1024];

	private String accessKeyId = null;
	private String secretKey = null;
    private String url = null;
		
	/**
	 * Constructs an instance of S3 Object to allow for operations against Amazon S3.
	 *  	
	 * @param url The access point of S3.  {@link #HTTP_URL} or {@link #HTTPS_URL}
	 * @param accessKeyId The Amazon AWS Access Key Id to use to access Amazon S3.
	 * @param secretKey The Amazon AWS Secret Key to use to access Amazon S3.
	 **/
	public S3( String url, String accessKeyId, String secretKey ) {
        this.url = url;
		this.accessKeyId = accessKeyId;
		this.secretKey = secretKey;
	}
	
	/**
	 * Implements the <b>List All My Buckets</b> operation described 
	 * <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/SOAPListAllMyBuckets.html">here.</a>
	 *
	 * @return {@link S3BucketList}
	 * @exception Exception Represents a communication error or one of the Amazon S3 error codes found <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/ErrorCodeList.html">here.</a>
	 */
	public S3BucketList listMyBuckets() throws Exception {
		String timestamp = this.getTimestamp();
		String signature = this.getSignature( "ListAllMyBuckets", timestamp );
		String soap = this.listMyBuckets( this.accessKeyId, timestamp, signature );		
 
        S3Result result = S3Result.parse( SoapRequest.processSoapRequest( this.url, soap ) );
        if ( result.wasSuccessful() ) {
            return S3BucketList.createBucketList( result.getResult() );
        }
        else {
            throw new Exception( result.getFaultCode() );
        }
	}
	
	/**
	 * Implements the <b>Create Bucket</b> operation described 
	 * <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/SOAPCreateBucket.html">here.</a>
	 *
	 * @param bucketName The name of the bucket to create.
	 * @exception Exception Represents a communication error or one of the Amazon S3 error codes found <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/ErrorCodeList.html">here.</a>
	 */
	public void createBucket( String bucketName ) throws Exception {
		String timestamp = this.getTimestamp();
		String signature = this.getSignature( "CreateBucket", timestamp );
		String soap = this.createBucket( bucketName, this.accessKeyId, timestamp, signature );		
 
        S3Result result = S3Result.parse( SoapRequest.processSoapRequest( this.url, soap ) );
        if ( !result.wasSuccessful() ) {
            throw new Exception( result.getFaultCode() );
        }
	}
	
	/**
	 * Implements the <b>Delete Bucket</b> operation described 
	 * <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/SOAPDeleteBucket.html">here.</a>
	 *
	 * @param bucketName The name of the bucket to delete.
	 * @exception Exception Represents a communication error or one of the Amazon S3 error codes found <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/ErrorCodeList.html">here.</a>
	 */
	public void deleteBucket( String bucketName ) throws Exception {
		String timestamp = this.getTimestamp();
		String signature = this.getSignature( "DeleteBucket", timestamp );
		String soap = this.deleteBucket( bucketName, this.accessKeyId, timestamp, signature );

        S3Result result = S3Result.parse( SoapRequest.processSoapRequest( this.url, soap ) );
        if ( !result.wasSuccessful() ) {
            throw new Exception( result.getFaultCode() );
        }
	}
	
	/**
	 * Implements the <b>List Bucket</b> operation described 
	 * <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/SOAPListBucket.html">here.</a>
	 *
	 * @param bucketName The name of the bucket to list.
	 *
	 * @return {@link S3Bucket}
	 * @exception Exception Represents a communication error or one of the Amazon S3 error codes found <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/ErrorCodeList.html">here.</a>
	 */
	public S3Bucket listBucket( String bucketName ) throws Exception {
		String timestamp = this.getTimestamp();
		String signature = this.getSignature( "ListBucket", timestamp );
		String soap = this.listBucket( bucketName, this.accessKeyId, timestamp, signature );		
        
        S3Result result = S3Result.parse( SoapRequest.processSoapRequest( this.url, soap ) );
        if ( result.wasSuccessful() ) {
            return S3Bucket.createBucket( bucketName, result.getResult() );
        }
        else {
            throw new Exception( result.getFaultCode() );
        }
	}
	
	/**
	 * Implements the <b>Put Object Inline</b> operation described 
	 * <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/SOAPPutObjectInline.html">here.</a>
	 *
	 * @param bucketName The name of the bucket to put the object into.
	 * @param key The name of of the object.
	 * @param data The data to put within the object.
	 * @exception Exception Represents a communication error or one of the Amazon S3 error codes found <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/ErrorCodeList.html">here.</a>
	 */
	public void putObjectInline( String bucketName, String key, String data ) throws Exception {
		String timestamp = this.getTimestamp();
		String signature = this.getSignature( "PutObjectInline", timestamp );
		String soap = this.putObjectInline( bucketName, key, data, String.valueOf( data.length() ), this.accessKeyId, timestamp, signature );

        S3Result result = S3Result.parse( SoapRequest.processSoapRequest( this.url, soap ) );
        if ( !result.wasSuccessful() ) {
            throw new Exception( result.getFaultCode() );
        }
	}
	
	/**
	 * Implements the <b>Delete Object</b> operation described 
	 * <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/SOAPDeleteObject.html">here.</a>
	 *
	 * @param bucketName The name of the bucket to delete the object from.
	 * @param key The name of the object to delete.
	 * @exception Exception Represents a communication error or one of the Amazon S3 error codes found <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/ErrorCodeList.html">here.</a>
	 */
	public void deleteObject( String bucketName, String key ) throws Exception { 
		String timestamp = this.getTimestamp();
		String signature = this.getSignature( "DeleteObject", timestamp );
		String soap = this.deleteObject( bucketName, key, this.accessKeyId, timestamp, signature );

        S3Result result = S3Result.parse( SoapRequest.processSoapRequest( this.url, soap ) );
        if ( !result.wasSuccessful() ) {
            throw new Exception( result.getFaultCode() );
        }
	}
	
	/**
	 * Implements the <b>Get Object</b> operation described 
	 * <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/SOAPGetObject.html">here.</a>
	 *
	 * @param bucketName The name of the bucket to get the object from.
	 * @param key The name of the object to return.
	 *
	 * @return {@link S3Object}
	 * @exception Exception Represents a communication error or one of the Amazon S3 error codes found <a href="http://docs.amazonwebservices.com/AmazonS3/2006-03-01/ErrorCodeList.html">here.</a>
	 */
	public S3Object getObject( String bucketName, String key ) throws Exception {
		String timestamp = this.getTimestamp();
		String signature = this.getSignature( "GetObject", timestamp );
		String soap = this.getObject( bucketName, key, this.accessKeyId, timestamp, signature );

        S3Result result = S3Result.parse( SoapRequest.processSoapRequest( this.url, soap ) );
        if ( result.wasSuccessful() ) {
            return S3Object.createObject( key, result.getResult() );
        }
        else {
            throw new Exception( result.getFaultCode() );
        }
	}

	protected String getTimestamp() {
		return DateFormatter.formatTime( Calendar.getInstance( S3Constants.GMT ) );
	}

	protected String getSignature( String operation, String timestamp ) {	
		return HMACSigner.sign( this.secretKey, "AmazonS3", operation, timestamp );
	}
	
	protected static String listMyBuckets( String accessKeyId, String timestamp, String signature ) {
		StringBuffer buffer = new StringBuffer( S3Constants.LIST_MY_BUCKETS );

		replace( buffer, S3Constants.LIST_MY_BUCKETS, S3Constants.SIGNATURE, signature );
		replace( buffer, S3Constants.LIST_MY_BUCKETS, S3Constants.TIMESTAMP, timestamp );
		replace( buffer, S3Constants.LIST_MY_BUCKETS, S3Constants.ACCESS_KEY_ID, accessKeyId );
	
		return buffer.toString();
	}
	
	protected static String createBucket( String bucketName, String accessKeyId, String timestamp, String signature ) {
		StringBuffer buffer = new StringBuffer( S3Constants.CREATE_BUCKET );

		replace( buffer, S3Constants.CREATE_BUCKET, S3Constants.SIGNATURE, signature );
		replace( buffer, S3Constants.CREATE_BUCKET, S3Constants.TIMESTAMP, timestamp );
		replace( buffer, S3Constants.CREATE_BUCKET, S3Constants.ACCESS_KEY_ID, accessKeyId );
		replace( buffer, S3Constants.CREATE_BUCKET, S3Constants.BUCKET_NAME, bucketName );
	
		return buffer.toString();
	}
	
	protected static String deleteBucket( String bucketName, String accessKeyId, String timestamp, String signature ) {
		StringBuffer buffer = new StringBuffer( S3Constants.DELETE_BUCKET );

		replace( buffer, S3Constants.DELETE_BUCKET, S3Constants.SIGNATURE, signature );
		replace( buffer, S3Constants.DELETE_BUCKET, S3Constants.TIMESTAMP, timestamp );
		replace( buffer, S3Constants.DELETE_BUCKET, S3Constants.ACCESS_KEY_ID, accessKeyId );
		replace( buffer, S3Constants.DELETE_BUCKET, S3Constants.BUCKET_NAME, bucketName );
	
		return buffer.toString();
	}
	
	protected static String listBucket( String bucketName, String accessKeyId, String timestamp, String signature ) {
		StringBuffer buffer = new StringBuffer( S3Constants.LIST_BUCKET );

		replace( buffer, S3Constants.LIST_BUCKET, S3Constants.SIGNATURE, signature );
		replace( buffer, S3Constants.LIST_BUCKET, S3Constants.TIMESTAMP, timestamp );
		replace( buffer, S3Constants.LIST_BUCKET, S3Constants.ACCESS_KEY_ID, accessKeyId );
		replace( buffer, S3Constants.LIST_BUCKET, S3Constants.BUCKET_NAME, bucketName );
	
		return buffer.toString();
	}

	protected static String putObjectInline( String bucketName, String key, String data, String contentLength, String accessKeyId, String timestamp, String signature ) {
		StringBuffer buffer = new StringBuffer( S3Constants.PUT_OBJECT );

		replace( buffer, S3Constants.PUT_OBJECT, S3Constants.SIGNATURE, signature );
		replace( buffer, S3Constants.PUT_OBJECT, S3Constants.TIMESTAMP, timestamp );
		replace( buffer, S3Constants.PUT_OBJECT, S3Constants.ACCESS_KEY_ID, accessKeyId );
		replace( buffer, S3Constants.PUT_OBJECT, S3Constants.CONTENT_LENGTH, contentLength );
		replace( buffer, S3Constants.PUT_OBJECT, S3Constants.DATA, data );
		replace( buffer, S3Constants.PUT_OBJECT, S3Constants.OBJECT_KEY, key );
		replace( buffer, S3Constants.PUT_OBJECT, S3Constants.BUCKET_NAME, bucketName );
	
		return buffer.toString();
	}

	protected static String deleteObject( String bucketName, String key, String accessKeyId, String timestamp, String signature ) {
		StringBuffer buffer = new StringBuffer( S3Constants.DELETE_OBJECT );

		replace( buffer, S3Constants.DELETE_OBJECT, S3Constants.SIGNATURE, signature );
		replace( buffer, S3Constants.DELETE_OBJECT, S3Constants.TIMESTAMP, timestamp );
		replace( buffer, S3Constants.DELETE_OBJECT, S3Constants.ACCESS_KEY_ID, accessKeyId );
		replace( buffer, S3Constants.DELETE_OBJECT, S3Constants.OBJECT_KEY, key );
		replace( buffer, S3Constants.DELETE_OBJECT, S3Constants.BUCKET_NAME, bucketName );
	
		return buffer.toString();
	}

	protected static String getObject( String bucketName, String key, String accessKeyId, String timestamp, String signature ) {
		StringBuffer buffer = new StringBuffer( S3Constants.GET_OBJECT );

		replace( buffer, S3Constants.GET_OBJECT, S3Constants.SIGNATURE, signature );
		replace( buffer, S3Constants.GET_OBJECT, S3Constants.TIMESTAMP, timestamp );
		replace( buffer, S3Constants.GET_OBJECT, S3Constants.ACCESS_KEY_ID, accessKeyId );
		replace( buffer, S3Constants.GET_OBJECT, S3Constants.OBJECT_KEY, key );
		replace( buffer, S3Constants.GET_OBJECT, S3Constants.BUCKET_NAME, bucketName );
	
		return buffer.toString();
	}

	protected static void replace( StringBuffer buffer, String original, String token, String value ) {
		int startIndex = original.indexOf( token );
		buffer.delete( startIndex, startIndex + token.length() );
		buffer.insert( startIndex, value );
	}
    
}
