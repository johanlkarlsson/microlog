
/*
 * This software code is made available "AS IS" without warranties of any 
 * kind.  You may copy, display, modify and redistribute the software
 * code either by itself or as incorporated into your code; provided that
 * you do not remove any proprietary notices.  Your use of this software
 * code is at your own risk and you waive any claim against Amazon
 * Web Services LLC or its affiliates with respect to your use of
 * this software code. (c) Amazon Web Services LLC or its
 * affiliates.
 */


package com.amazonaws.s3;

public class S3Result {

    private static final String SOAP_FAULT_ELEMENT_START = "<soapenv:Fault>";
    private static final String SOAP_FAULT_ELEMENT_END = "</soapenv:Fault>";

    private static final String SOAP_BODY_ELEMENT_START = "<soapenv:Body>";
    private static final String SOAP_BODY_ELEMENT_END = "</soapenv:Body>";

    private static final String FAULT_CODE_ELEMENT_START = "<faultcode>";
    private static final String FAULT_CODE_ELEMENT_END = "</faultcode>";

    private static final String FAULT_STRING_ELEMENT_START = "<faultstring>";
    private static final String FAULT_STRING_ELEMENT_END = "</faultstring>";


    private String faultCode = null;
    private String faultString = null;
    private String result = null;

    public static S3Result parse( String soap ) {
        String result = null;
        String faultCode = null;
        String faultString = null;
    
        if ( soap.indexOf( SOAP_FAULT_ELEMENT_START ) != -1 ) {
            result = soap.substring( soap.indexOf( SOAP_FAULT_ELEMENT_START ) + SOAP_FAULT_ELEMENT_START.length(), soap.indexOf( SOAP_FAULT_ELEMENT_END ) );
    		faultCode = soap.substring( soap.indexOf( FAULT_CODE_ELEMENT_START ) + FAULT_CODE_ELEMENT_START.length(), soap.indexOf( FAULT_CODE_ELEMENT_END ) );
            faultString = soap.substring( soap.indexOf( FAULT_STRING_ELEMENT_START ) + FAULT_STRING_ELEMENT_START.length(), soap.indexOf( FAULT_STRING_ELEMENT_END ) );
        }
        else {
            result = soap.substring( soap.indexOf( SOAP_BODY_ELEMENT_START ) + SOAP_BODY_ELEMENT_START.length(), soap.indexOf( SOAP_BODY_ELEMENT_END ) );
    		faultCode = null;
            faultString = null;
        }
        
        return new S3Result( result, faultCode, faultString );
    }

    protected S3Result( String result, String code, String message ) {
        this.result = result;
        this.faultCode = code;
        this.faultString = message;
    }
    
    public String getResult() {
        return this.result;
    }

    public String getFaultCode() {
        return this.faultCode;
    }
    
    public String getFaultString() {
        return this.faultString;
    }

    public boolean wasSuccessful() {
        return ( this.faultCode == null );
    }
    
    public String toString() {
        return "{ Result = [" + this.result + "], Fault Code = [" + this.faultCode + "], Fault String = [" + this.faultString + "] }";
    }
}


    
