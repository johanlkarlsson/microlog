
/*
 * This software code is made available "AS IS" without warranties of any 
 * kind.  You may copy, display, modify and redistribute the software
 * code either by itself or as incorporated into your code; provided that
 * you do not remove any proprietary notices.  Your use of this software
 * code is at your own risk and you waive any claim against Amazon
 * Web Services LLC or its affiliates with respect to your use of
 * this software code. (c) Amazon Web Services LLC or its
 * affiliates.
 */


package amazon.soap;

import javax.microedition.io.HttpConnection;
import javax.microedition.io.Connector;

import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public class SoapRequest {
    
    public static String processSoapRequest( String urlAddress, String soapRequest ) throws Exception {
		HttpConnection conn = null;
		InputStream inputStream = null;
        OutputStream out = null;
        ByteArrayOutputStream baos = null;
        
		try {
		    byte[] xmlBytes = soapRequest.getBytes();
            
			conn = ( HttpConnection ) Connector.open( urlAddress );
			conn.setRequestProperty( "Content-Length", String.valueOf( xmlBytes.length ) );
			conn.setRequestProperty( "Content-Type", "text/xml; charset=utf-8" );
			conn.setRequestProperty( "SOAPAction", "SOAP" );
			conn.setRequestMethod( "POST" );

			out = conn.openOutputStream();
			out.write( xmlBytes );
			out.close();
                        
			baos = new ByteArrayOutputStream( 8196 );   
            byte[] data = new byte[ 1024 ];
            inputStream = conn.openInputStream();
			int length = 0;
            while ( ( length = inputStream.read( data ) ) != -1 ) {
                baos.write( data, 0, length );
            }  
            
			return baos.toString();
		}
        finally {
 			try { 
                if ( inputStream != null ) 
                    inputStream.close(); 
            } 
            catch ( Exception exception ) {}
            
            try { 
                if ( out != null ) 
                    out.close(); 
            } 
            catch ( Exception exception ) {}
            
			try { 
                if ( conn != null ) 
                    conn.close(); 
            } 
            catch ( Exception exception ) {}
           
		    try { 
                if ( baos != null ) 
                    baos.close(); 
            } 
            catch ( Exception exception ) {}
                
        }
    }
}
