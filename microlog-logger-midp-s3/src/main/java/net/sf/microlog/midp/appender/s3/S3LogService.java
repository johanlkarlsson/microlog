/*
 * Copyright 2008 The Microlog project @sourceforge.net
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sf.microlog.midp.appender.s3;

/**
 * An interface that is used for accessing S3.
 * 
 * @author Johan Karlsson (johan.karlsson@jayway.se)
 */
public interface S3LogService {

	/**
	 * Set the access key ID for your Amazon Web Service (AWS) account.
	 * 
	 * @param accessKeyID
	 *            the access key id to use.
	 * @throws IllegalArgumentException if the accessKeyID is null.
	 */
	public void setAccessKeyID(String accessKeyID);

	/**
	 * Set the secret access key for your Amazon Web Service (AWS) account.
	 * 
	 * @param secretAccessKey
	 *            the access key to use.
	 * @throws IllegalArgumentException if the secretAccessKey is null.
	 */
	public void setSecretAccessKey(String secretAccessKey);

	/**
	 * Store the data on S3 with specified name.
	 * 
	 * @throws IllegalArgumentException if name or data is null.
	 */
	public void storeLog(String name, byte[] data);

}
